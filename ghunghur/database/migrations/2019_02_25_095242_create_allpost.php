<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllpost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allpost', function (Blueprint $table) {
            $table->increments('id');
            $table->string('author_name');
            $table->string('title');
            $table->string('published_date');
            $table->text('post_body');
            $table->dateTime('date');
            $table->integer('status');
            $table->string('image');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allpost');
    }
}

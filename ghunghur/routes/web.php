<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('users', 'Admin\UserController');

Auth::routes();

Route::get('admin/login', 'ArticaleController@index', function(){})->name('admin.login');

Route::any('admin/logout', 'Admin\LoginController@logout')->name('admin');

Route::get('logout', 'Auth\LoginController@logout');

      /* ------------- All Web Route ----------------------*/

Route::get('/', 'WebController\HomeController@home')->name('home');

Route::GET('/home','WebController\HomeController@home')->name('home');

Route::GET('/article','WebController\ArticaleController@allArticale')->name('article');

Route::GET('showArticle/{id}','WebController\ArticaleController@showArticle')->name('showArticle');

Route::GET('/poem', 'WebController\PoemController@allPoem')->name('poem');
Route::GET('showPoem/{id}','WebController\PoemController@showPoem')->name('showPoem');

Route::GET('/story','WebController\StoryController@allStory')->name('story');

Route::GET('showStory/{id}','WebController\StoryController@showStory')->name('showStory');

Route::get('/series','WebController\SeriesController@allSeries')->name('series');
Route::get('showSeries/{id}','WebController\SeriesController@showSeries')->name('showSeries');

Route::get('/interview','WebController\InterviewController@allInterview')->name('interview');

Route::get('showInterview/{id}','WebController\InterviewController@showInterview')->name('showInterview');

Route::get('/painting','WebController\PaintingController@allPainting')->name('painting');
Route::get('showPainting/{id}', 'WebController\PaintingController@showPainting')->name('showPainting');

Route::get('/translate','WebController\TranslationController@allTranslate')->name('translate');

Route::get('showTranslate/{id}', 'WebController\TranslationController@showTranslate')->name('showTranslate');

Route::get('/movie','WebController\MovieController@allMovie')->name('movie');

Route::get('showMovie/{id}', 'WebController\MovieController@showMovie')->name('showMovie');

Route::get('/drama','WebController\DramaController@allDrama')->name('drama');

Route::get('showDrama/{id}', 'WebController\DramaController@showDrama')->name('showDrama');


Route::get('/Muktogdy','WebController\MuktoGdyController@Muktogdy')->name('Muktogdy');

Route::get('showMuktogdyo/{id}', 'WebController\MuktoGdyController@showMuktogdy')->name('showMuktogdy');

Route::get('/SahitySongbad','WebController\SahitySongbadController@SahitySongbad')->name('SahitySongbad');

Route::get('showSahitySongbad/{id}', 'WebController\SahitySongbadController@showSahitySongbad')->name('showSahitySongbad');


Route::get('/rehearse','WebController\RehearseController@rehearse')->name('rehearse');

Route::get('showRehearse/{id}', 'WebController\RehearseController@showRehearse')->name('showRehearse');


Route::get('/rhyme','WebController\RhymeController@rhyme')->name('rhyme');

Route::get('showRhyme/{id}', 'WebController\RhymeController@showRhyme')->name('showRhyme');


Route::get('/others','WebController\OthersController@others')->name('others');

Route::get('showOthers/{id}', 'WebController\OthersController@showOthers')->name('showOthers');


Route::get('/books','WebController\BooksController@books')->name('books');

Route::get('showBooks/{id}', 'WebController\BooksController@showBooks')->name('showBooks');


Route::get('/special','WebController\SpecialVoume@special')->name('special');

Route::get('showSpecialVolume/{id}', 'WebController\SpecialVoume@showSpecialVolume')->name('showSpecialVolume');

Route::get('/bookHouse','WebController\BookHouseController@bookHouse')->name('bookHouse');

Route::get('/Archive','WebController\ArchiveController@Archive')->name('Archive');

       /*--------------  Route For Search ---------------*/

Route::get('/search', 'WebController\SearchController@index')->name('search');

Route::get('/mobilesearch', 'WebController\SearchController@mobilesearch')->name('mobilesearch');





      /* -------------All Route For Article --------------*/

Route::GET('admin/home','ArticaleController@index');
Route::GET('admin/client','ClientController@index');

Route::get('admin/Artical','ArticaleController@Artical')->name('admin.Artical');
Route::GET('admin/AllArtical','ArticaleController@AllArtical')->name('admin.AllArtical');
Route::PUT('update/{id}','ArticaleController@update')->name('admin.update');
Route::get('update/{id}','ArticaleController@edit')->name('admin.update');
Route::GET('view/{id}','ArticaleController@show')->name('admin.view');
Route::GET('destroy/{id}','ArticaleController@destroy')->name('admin.destroy');

Route::post('admin/CreateArtical','ArticaleController@CreateArtical')->name('admin.CreateArtical');
Route::get('admin/getAllArticle', 'ArticaleController@getAllArticle')->name('getAllArticle');
Route::get('unpublished/{id}', 'ArticaleController@unpublished')->name('admin.unpublished');
Route::get('published/{id}', 'ArticaleController@published')->name('admin.published');

/* -------------All Route For Poeam --------------*/

Route::get('admin/AddPoeams', 'PoeamsController@AddPoeams')->name('admin.AddPoeams');
Route::post('admin/StotePoeams', 'PoeamsController@StotePoeams')->name('admin.StotePoeams');
Route::get('admin/AllPoeams', 'PoeamsController@AllPoeams')->name('AllPoeams');
Route::get('admin/getAllPoeams', 'PoeamsController@getAllPoeams')->name('getAllPoeams');
Route::get('destroyPoeams/{id}', 'PoeamsController@destroyPoeams')->name('admin.destroyPoeams');
Route::PUT('updatePoeam/{id}', 'PoeamsController@updatePoeam')->name('admin.updatePoeam');
Route::get('editPoeam/{id}','PoeamsController@editPoeam')->name('admin.editPoeam');
Route::GET('viewPoeams/{id}','PoeamsController@viewPoeams')->name('admin.viewPoeams');
Route::get('unpublishedPoeam/{id}', 'PoeamsController@unpublishedPoeam')->name('admin.unpublishedPoeam');
Route::get('publishedPoeam/{id}', 'PoeamsController@publishedPoeam')->name('admin.publishedPoeam');

/* -------------All Route For Short Story --------------*/

Route::get('admin/AddStory', 'StoryController@AddStory')->name('admin.AddStory');
Route::post('admin/StoreStory', 'StoryController@StoreStory')->name('admin.StoreStory');
Route::get('admin/AllStory', 'StoryController@AllStory')->name('AllStory');
Route::get('admin/getAllStory', 'StoryController@getAllStory')->name('getAllStory');
Route::get('destroyStory/{id}', 'StoryController@destroyStory')->name('admin.destroyStory');
Route::PUT('updateStory/{id}', 'StoryController@updateStory')->name('admin.updateStory');
Route::get('editStory/{id}','StoryController@editStory')->name('admin.editStory');
Route::GET('viewStory/{id}','StoryController@viewStory')->name('admin.viewStory');
Route::get('unpublishedStory/{id}', 'StoryController@unpublishedStory')->name('admin.unpublishedStory');
Route::get('publishedStory/{id}', 'StoryController@publishedStory')->name('admin.publishedStory');


/* -------------All Route For Series --------------*/

Route::get('admin/AddSeries', 'SeriesController@AddSeries')->name('admin.AddSeries');
Route::post('admin/StoreSeries', 'SeriesController@StoreSeries')->name('admin.StoreSeries');
Route::get('admin/AllSeries', 'SeriesController@AllSeries')->name('AllSeries');
Route::get('admin/getAllSeries', 'SeriesController@getAllSeries')->name('getAllSeries');
Route::get('destroySeries/{id}', 'SeriesController@destroySeries')->name('admin.destroySeries');
Route::PUT('updateSeries/{id}', 'SeriesController@updateSeries')->name('admin.updateSeries');
Route::get('editSeries/{id}','SeriesController@editSeries')->name('admin.editSeries');
Route::GET('viewSeries/{id}','SeriesController@viewSeries')->name('admin.viewSeries');
Route::get('unpublishedSeries/{id}', 'SeriesController@unpublishedSeries')->name('admin.unpublishedSeries');
Route::get('publishedSeries/{id}', 'SeriesController@publishedSeries')->name('admin.publishedSeries');


/* ------------ All Route For Interview -------------- */

Route::get('admin/AddInterview', 'InterviewController@AddInterview')->name('admin.AddInterview');
Route::post('admin/StoreInterview', 'InterviewController@StoreInterview')->name('admin.StoreInterview');
Route::get('admin/AllInterview', 'InterviewController@AllInterview')->name('AllInterview');
Route::get('admin/getAllInterview', 'InterviewController@getAllInterview')->name('getAllInterview');
Route::get('destroyInterview/{id}', 'InterviewController@destroyInterview')->name('admin.destroyInterview');
Route::PUT('updateInterview/{id}', 'InterviewController@updateInterview')->name('admin.updateInterview');
Route::get('editInterview/{id}','InterviewController@editInterview')->name('admin.editInterview');
Route::GET('viewInterview/{id}','InterviewController@viewInterview')->name('admin.viewInterview');
Route::get('unpublishedInterview/{id}', 'InterviewController@unpublishedInterview')->name('admin.unpublishedInterview');
Route::get('publishedInterview/{id}', 'InterviewController@publishedInterview')->name('admin.publishedInterview');


/* ----------- All Route For Renderings  -------------  */

Route::get('admin/AddTranslation', 'TranslationController@AddTranslation')->name('admin.AddTranslation');
Route::post('admin/StoreTranslation', 'TranslationController@StoreTranslation')->name('admin.StoreTranslation');
Route::get('admin/AllTranslation', 'TranslationController@AllTranslation')->name('AllTranslation');
Route::get('admin/getAllTranslation', 'TranslationController@getAllTranslation')->name('getAllTranslation');
Route::get('destroyTranslation/{id}', 'TranslationController@destroyTranslation')->name('admin.destroyTranslation');
Route::PUT('updateTranslation/{id}', 'TranslationController@updateTranslation')->name('admin.updateTranslation');
Route::get('editTranslation/{id}','TranslationController@editTranslation')->name('admin.editTranslation');
Route::GET('viewTranslation/{id}','TranslationController@viewTranslation')->name('admin.viewTranslation');
Route::get('unpublishedTranslation/{id}', 'TranslationController@unpublishedTranslation')->name('admin.unpublishedTranslation');
Route::get('publishedTranslation/{id}', 'TranslationController@publishedTranslation')->name('admin.publishedTranslation');

/* ---------- All Route For Movies ---------------------  */

Route::get('admin/AddMovie', 'MovieController@AddMovie')->name('admin.AddMovie');
Route::post('admin/StoreMovie', 'MovieController@StoreMovie')->name('admin.StoreMovie');
Route::get('admin/AllMovies', 'MovieController@AllMovies')->name('AllMovies');
Route::get('admin/getAllMovies', 'MovieController@getAllMovies')->name('getAllMovies');
Route::get('destroyMovie/{id}', 'MovieController@destroyMovie')->name('admin.destroyMovie');
Route::get('editMovie/{id}','MovieController@editMovie')->name('admin.editMovie');
Route::PUT('updateMovie/{id}', 'MovieController@updateMovie')->name('admin.updateMovie');
Route::GET('viewMovie/{id}','MovieController@viewMovie')->name('admin.viewMovie');
Route::get('unpublishedMovie/{id}', 'MovieController@unpublishedMovie')->name('admin.unpublishedMovie');
Route::get('publishedMovie/{id}', 'MovieController@publishedMovie')->name('admin.publishedMovie');


/* ---------- All Route For Drama ---------------------  */

Route::get('admin/AddDrama', 'DramaController@AddDrama')->name('admin.AddDrama');
Route::post('admin/StoreDrama', 'DramaController@StoreDrama')->name('admin.StoreDrama');
Route::get('admin/AllDrama', 'DramaController@AllDrama')->name('AllDrama');
Route::get('admin/getAllDrama', 'DramaController@getAllDrama')->name('getAllDrama');
Route::get('destroyDrama/{id}', 'DramaController@destroyDrama')->name('admin.destroyDrama');
Route::get('editDrama/{id}','DramaController@editDrama')->name('admin.editDrama');
Route::PUT('updateDrama/{id}', 'DramaController@updateDrama')->name('admin.updateDrama');
Route::GET('viewDrama/{id}','DramaController@viewDrama')->name('admin.viewDrama');
Route::get('unpublishedDrama/{id}', 'DramaController@unpublishedDrama')->name('admin.unpublishedDrama');
Route::get('publishedDrama/{id}', 'DramaController@publishedDrama')->name('admin.publishedDrama');


/* ---------- All Route For Painting ---------------------  */

Route::get('admin/AddPainting', 'PaintingController@AddPainting')->name('admin.AddPainting');
Route::post('admin/StorePainting', 'PaintingController@StorePainting')->name('admin.StorePainting');

Route::get('admin/AllPainting', 'PaintingController@AllPainting')->name('AllPainting');
Route::get('admin/getAllPainting', 'PaintingController@getAllPainting')->name('getAllPainting');
Route::get('destroyPainting/{id}', 'PaintingController@destroyPainting')->name('admin.destroyPainting');
Route::get('editPainting/{id}','PaintingController@editPainting')->name('admin.editPainting');
Route::PUT('updatePainting/{id}', 'PaintingController@updatePainting')->name('admin.updatePainting');
Route::GET('viewPainting/{id}','PaintingController@viewPainting')->name('admin.viewPainting');

Route::get('unpublishedPainting/{id}', 'PaintingController@unpublishedPainting')->name('admin.unpublishedPainting');
Route::get('publishedPainting/{id}', 'PaintingController@publishedPainting')->name('admin.publishedPainting');


/* ---------- All Route For Muktogdyo ---------------------  */

Route::get('admin/AddMuktogdyo', 'MuktogdyoController@AddMuktogdyo')->name('admin.AddMuktogdyo');
Route::post('admin/StoreMuktogdyo', 'MuktogdyoController@StoreMuktogdyo')->name('admin.StoreMuktogdyo');
Route::get('admin/AllMuktogdyo', 'MuktogdyoController@AllMuktogdyo')->name('AllMuktogdyo');
Route::get('admin/getAllMuktogdyo', 'MuktogdyoController@getAllMuktogdyo')->name('getAllMuktogdyo');
Route::get('destroyMuktogdyo/{id}', 'MuktogdyoController@destroyMuktogdyo')->name('admin.destroyMuktogdyo');
Route::get('editMuktogdyo/{id}','MuktogdyoController@editMuktogdyo')->name('admin.editMuktogdyo');
Route::PUT('updateMuktogdyo/{id}', 'MuktogdyoController@updateMuktogdyo')->name('admin.updateMuktogdyo');
Route::GET('viewMuktogdyo/{id}','MuktogdyoController@viewMuktogdyo')->name('admin.viewMuktogdyo');
Route::get('unpublishedMuktogdyo/{id}', 'MuktogdyoController@unpublishedMuktogdyo')->name('admin.unpublishedMuktogdyo');
Route::get('publishedMuktogdyo/{id}', 'MuktogdyoController@publishedMuktogdyo')->name('admin.publishedMuktogdyo');


/* ---------- All Route For সাহিত্য সংবাদ   ---------------------  */

Route::get('admin/AddSahitySongbad', 'SahitySongbadController@AddSahitySongbad')->name('admin.AddSahitySongbad');
Route::post('admin/StoreSahitySongbad', 'SahitySongbadController@StoreSahitySongbad')->name('admin.StoreSahitySongbad');
Route::get('admin/AllSahitySongbad', 'SahitySongbadController@AllSahitySongbad')->name('AllSahitySongbad');
Route::get('admin/getAllSahitySongbad', 'SahitySongbadController@getAllSahitySongbad')->name('getAllSahitySongbad');
Route::get('destroySahitySongbad/{id}', 'SahitySongbadController@destroySahitySongbad')->name('admin.destroySahitySongbad');
Route::get('editSahitySongbad/{id}','SahitySongbadController@editSahitySongbad')->name('admin.editSahitySongbad');
Route::PUT('updateSahitySongbad/{id}', 'SahitySongbadController@updateSahitySongbad')->name('admin.updateSahitySongbad');
Route::GET('viewSahitySongbad/{id}','SahitySongbadController@viewSahitySongbad')->name('admin.viewSahitySongbad');
Route::get('unpublishedSahitySongbad/{id}', 'SahitySongbadController@unpublishedSahitySongbad')->name('admin.unpublishedSahitySongbad');
Route::get('publishedSahitySongbad/{id}', 'SahitySongbadController@publishedSahitySongbad')->name('admin.publishedSahitySongbad');

/* ---------- All Route For বিশেষ সংখ্যা  ---------------------  */

Route::get('admin/AddSpecialVolume', 'SpecialVolumeController@AddSpecialVolume')->name('admin.AddSpecialVolume');
Route::post('admin/StoreSpecialVolume', 'SpecialVolumeController@StoreSpecialVolume')->name('admin.StoreSpecialVolume');
Route::get('admin/AllSpecialVolume', 'SpecialVolumeController@AllSpecialVolume')->name('AllSpecialVolume');
Route::get('admin/getSpecialVolume', 'SpecialVolumeController@getSpecialVolume')->name('getSpecialVolume');
Route::get('destroySpecialVolume/{id}', 'SpecialVolumeController@destroySpecialVolume')->name('admin.destroySpecialVolume');
Route::get('destroySpecialFile/{id}', 'SpecialVolumeController@destroySpecialFile')->name('admin.destroySpecialFile');
Route::get('editSpecialVolume/{id}','SpecialVolumeController@editSpecialVolume')->name('admin.editSpecialVolume');
Route::PUT('updateSpecialVolume/{id}', 'SpecialVolumeController@updateSpecialVolume')->name('admin.updateSpecialVolume');
Route::GET('viewSpecialVolume/{id}','SpecialVolumeController@viewSpecialVolume')->name('admin.viewSpecialVolume');
Route::get('unpublishedSpecialVolume/{id}', 'SpecialVolumeController@unpublishedSpecialVolume')->name('admin.unpublishedSpecialVolume');
Route::get('publishedSpecialVolume/{id}', 'SpecialVolumeController@publishedSpecialVolume')->name('admin.publishedSpecialVolume');
Route::get('admin/SpecialVolumeList', 'SpecialVolumeController@SpecialVolumeList')->name('SpecialVolumeList');
Route::get('admin/getAllSpecialVolume', 'SpecialVolumeController@getAllSpecialVolume')->name('getAllSpecialVolume');


/* ---------- All Route For পুনর্পাঠ    ---------------------  */

Route::get('admin/AddRehearse', 'RehearseController@AddRehearse')->name('admin.AddRehearse');
Route::post('admin/StoreAddRehearse', 'RehearseController@StoreAddRehearse')->name('admin.StoreAddRehearse');
Route::get('admin/AllRehearse', 'RehearseController@AllRehearse')->name('AllRehearse');
Route::get('admin/getAllRehearse', 'RehearseController@getAllRehearse')->name('getAllRehearse');
Route::get('destroyRehearse/{id}', 'RehearseController@destroyRehearse')->name('admin.destroyRehearse');
Route::get('editRehearse/{id}','RehearseController@editRehearse')->name('admin.editRehearse');
Route::PUT('updateRehearse/{id}', 'RehearseController@updateRehearse')->name('admin.updateRehearse');
Route::GET('viewRehearse/{id}','RehearseController@viewRehearse')->name('admin.viewRehearse');
Route::get('unpublishedRehearse/{id}', 'RehearseController@unpublishedRehearse')->name('admin.unpublishedRehearse');
Route::get('publishedRehearse/{id}', 'RehearseController@publishedRehearse')->name('admin.publishedRehearse');


/* ---------- All Route For বইপত্র     ---------------------  */

Route::get('admin/AddBooks', 'BooksController@AddBooks')->name('admin.AddBooks');
Route::post('admin/StoreBooks', 'BooksController@StoreBooks')->name('admin.StoreBooks');
Route::get('admin/AllBooks', 'BooksController@AllBooks')->name('AllBooks');
Route::get('admin/getAllBooks', 'BooksController@getAllBooks')->name('getAllBooks');
Route::get('destroyBooks/{id}', 'BooksController@destroyBooks')->name('admin.destroyBooks');
Route::get('editBooks/{id}','BooksController@editBooks')->name('admin.editBooks');
Route::PUT('updateBooks/{id}', 'BooksController@updateBooks')->name('admin.updateBooks');
Route::GET('viewBooks/{id}','BooksController@viewBooks')->name('admin.viewBooks');
Route::get('unpublishedBooks/{id}', 'BooksController@unpublishedBooks')->name('admin.unpublishedBooks');
Route::get('publishedBooks/{id}', 'BooksController@publishedBooks')->name('admin.publishedBooks');

/* ---------- All Route For ছড়া     ---------------------  */

Route::get('admin/AddRhyme', 'RhymeController@AddRhyme')->name('admin.AddRhyme');
Route::post('admin/StoreRhyme', 'RhymeController@StoreRhyme')->name('admin.StoreRhyme');

Route::get('admin/AllRhyme', 'RhymeController@AllRhyme')->name('AllRhyme');
Route::get('admin/getAllRhyme', 'RhymeController@getAllRhyme')->name('getAllRhyme');

Route::get('destroyRhyme/{id}', 'RhymeController@destroyRhyme')->name('admin.destroyRhyme');
Route::get('editRhyme/{id}','RhymeController@editRhyme')->name('admin.editRhyme');
Route::PUT('updateRhyme/{id}', 'RhymeController@updateRhyme')->name('admin.updateRhyme');

Route::GET('viewRhyme/{id}','RhymeController@viewRhyme')->name('admin.viewRhyme');

Route::get('unpublishedRhyme/{id}', 'RhymeController@unpublishedRhyme')->name('admin.unpublishedRhyme');
Route::get('publishedRhyme/{id}', 'RhymeController@publishedRhyme')->name('admin.publishedRhyme');


/* ---------- All Route For অন্যান     ---------------------  */

Route::get('admin/AddOthers', 'OthersController@AddOthers')->name('admin.AddOthers');
Route::post('admin/StoreOthers', 'OthersController@StoreOthers')->name('admin.StoreOthers');

Route::get('admin/AllOthers', 'OthersController@AllOthers')->name('AllOthers');
Route::get('admin/getAllOthers', 'OthersController@getAllOthers')->name('getAllOthers');

Route::get('destroyOthers/{id}', 'OthersController@destroyOthers')->name('admin.destroyOthers');
Route::get('editOthers/{id}','OthersController@editOthers')->name('admin.editOthers');
Route::PUT('updateOthers/{id}', 'OthersController@updateOthers')->name('admin.updateOthers');

Route::GET('viewOthers/{id}','OthersController@viewOthers')->name('admin.viewOthers');

Route::get('unpublishedOthers/{id}', 'OthersController@unpublishedOthers')->name('admin.unpublishedOthers');
Route::get('publishedOthers/{id}', 'OthersController@publishedOthers')->name('admin.publishedOthers');


/* ---------- All Route For বই ঘর     ---------------------  */

Route::get('admin/AddBookHouse', 'BookHouse@AddBookHouse')->name('admin.AddBookHouse');
Route::post('admin/StoreBookHouse', 'BookHouse@StoreBookHouse')->name('admin.StoreBookHouse');

Route::get('admin/AllBookHouse', 'BookHouse@AllBookHouse')->name('AllBookHouse');
Route::get('admin/getAllBookHouse', 'BookHouse@getAllBookHouse')->name('getAllBookHouse');

Route::get('destroyBookHouse/{id}', 'BookHouse@destroyBookHouse')->name('admin.destroyBookHouse');

Route::get('editBookHouse/{id}','BookHouse@editBookHouse')->name('admin.editBookHouse');
Route::PUT('updateBookHouse/{id}', 'BookHouse@updateBookHouse')->name('admin.updateBookHouse');

Route::get('unpublishedBookhouse/{id}', 'BookHouse@unpublishedBookhouse')->name('admin.unpublishedBookhouse');
Route::get('publishedBookHouse/{id}', 'BookHouse@publishedBookHouse')->name('admin.publishedBookHouse');


/* ---------- All Route For ই-ঘুংঘুর     ---------------------  */

Route::get('admin/AddArchive', 'ArchiveController@AddArchive')->name('admin.AddArchive');
Route::post('admin/StoreArchive', 'ArchiveController@StoreArchive')->name('admin.StoreArchive');

Route::get('admin/AllArchive', 'ArchiveController@AllArchive')->name('AllArchive');
Route::get('admin/getAllArchive', 'ArchiveController@getAllArchive')->name('getAllArchive');

Route::get('destroyArchive/{id}', 'ArchiveController@destroyArchive')->name('admin.destroyArchive');

Route::get('unpublishedArchive/{id}', 'ArchiveController@unpublishedArchive')->name('admin.unpublishedArchive');
Route::get('publishedSArchive/{id}', 'ArchiveController@publishedSArchive')->name('admin.publishedSArchive');


/* ------------- Home Photography Upload -------------- */

Route::get('admin/fileCreate','HomePhotographController@fileCreate')->name('admin.fileCreate');
Route::post('admin/fileStore','HomePhotographController@fileStore');
Route::get('admin/viewHomePhotography','HomePhotographController@viewHomePhotography')->name('viewHomePhotography');
Route::get('admin/getHomePhotography','HomePhotographController@getHomePhotography')->name('getHomePhotography');
Route::get('fileDestroy{id}','HomePhotographController@fileDestroy')->name('admin.fileDestroy');


/* ------------- Home Painting Upload -------------- */

Route::get('admin/addPaintingImage','HomePaintingController@addPaintingImage')->name('admin.addPaintingImage');
Route::post('admin/storePainting','HomePaintingController@storePainting');
Route::get('admin/viewHomePainting','HomePaintingController@viewHomePainting')->name('viewHomePainting');
Route::get('admin/getHomePainting','HomePaintingController@getHomePainting')->name('getHomePainting');
Route::get('paintingDestroy{id}','HomePaintingController@paintingDestroy')->name('admin.paintingDestroy');


/* ------------- Advertisement Route ----------------- */

Route::get('admin/addAdvertising','AdvertisingController@addAdvertising')->name('admin.addAdvertising');
Route::post('admin/storeAdvertising','AdvertisingController@storeAdvertising')->name('admin.storeAdvertising');
Route::get('admin/viewAdvertising','AdvertisingController@viewAdvertising')->name('viewAdvertising');
Route::get('admin/getAllAdvertising','AdvertisingController@getAllAdvertising')->name('getAllAdvertising');
Route::get('advertiseDestroy{id}','AdvertisingController@advertiseDestroy')->name('admin.advertiseDestroy');


/* ------------- Image Link Route ----------------- */

Route::get('admin/addImage','ImageLinkController@addImage')->name('admin.addImage');
Route::post('admin/storeImage','ImageLinkController@storeImage')->name('admin.storeImage');
Route::get('admin/viewImageLink','ImageLinkController@viewImageLink')->name('viewImageLink');
Route::get('admin/getAllLink','ImageLinkController@getAllLink')->name('getAllLink');
Route::get('linkDestroy{id}','ImageLinkController@linkDestroy')->name('admin.linkDestroy');

/*----------------- Admin Route -------------------*/

/* ----------------- Admin Pass Change -------------*/

Route::get('admin/shoProfile','AdminController@shoProfile')->name('shoProfile');
Route::get('editProfile/{id}','AdminController@editProfile')->name('admin.editProfile');
Route::POST('updateProfile/{id}', 'AdminController@updateProfile')->name('admin.updateProfile');

Route::GET('admin','Admin\LoginController@showLoginForm')->name('admin.login');


Route::POST('admin','Admin\LoginController@login');

Route::POST('admin-password/email','Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::GET('admin-password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::POST('admin-password/reset','Admin\ResetPasswordController@reset');
Route::GET('admin-password/reset/{token}','Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

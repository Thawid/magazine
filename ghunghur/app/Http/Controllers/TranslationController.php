<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TranslationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddTranslation()
    {

        return view('admin.AddTranslation');
    }

    public function StoreTranslation(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddTranslation')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Translate'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddTranslation')->with('message', 'Translation Successfully Published');

    }


    public function AllTranslation()
    {
        $translation = DB::table('allpost')->where('type', '=', 'Translate')->get();
        return view('admin.AllTranslation', compact('translation'));
    }


    public function getAllTranslation(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $translation = DB::table('allpost')->where('type', '=', 'Translate')->get();
        $data = array();
        foreach ($translation as $allTranslation) {


            if ($allTranslation->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedTranslation', [$allTranslation->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedTranslation', [$allTranslation->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allTranslation->author_name, str_limit($allTranslation->title, 15), $allTranslation->published_date, str_limit($allTranslation->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Translate') . DIRECTORY_SEPARATOR . $allTranslation->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewTranslation', [$allTranslation->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editTranslation', [$allTranslation->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this Translate ?\');"  href="' . route('admin.destroyTranslation', [$allTranslation->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }

    public function destroyTranslation($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();

        File::delete(public_path('images/Translate' . $delete->image));
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllTranslation")->with('message', 'Translation Deleted Successfully !!');

    }


    public function editTranslation($id)
    {
        //
        $updateTranslation = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateTranslation', compact('updateTranslation'));
    }

    public function updateTranslation(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Translate'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Updated!!');
    }


    public function viewTranslation($id){

        $viewTranslation = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewTranslation', compact('viewTranslation'));
    }

    public function publishedTranslation($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllTranslation")->with('message', 'Successfully Published!!');
    }


    public function unpublishedTranslation($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllTranslation")->with('message', 'Successfully Unpublished!!');

    }

}

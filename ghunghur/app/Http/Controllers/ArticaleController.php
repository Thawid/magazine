<?php

namespace App\Http\Controllers;

use App\UserLoginHistory;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use App\LogUser;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use LiveControl\EloquentDataTable\DataTable;
use Illuminate\Support\Carbon;

class ArticaleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* return view('admin/home');*/
        return view('admin.home');
    }

    public function AllArtical()
    {


        $articles = DB::table('article')->get();
        return view('admin.AllArtical', compact('articles'));
    }

    public function Artical()
    {
        //
        return view('admin.CreateArtical');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function CreateArtical(Request $request)
    {


        $validatedData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validatedData->fails()) {
            return redirect('admin/Artical')
                ->withErrors($validatedData)
                ->withInput();
        }
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Article'), $imageName);

        $inputs = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'favourite' => $request['favourite'],
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];

        //dd($inputs);
        DB::table('allpost')->insert($inputs);
        return redirect()->back()->with('message', 'Successfully Published New Article!!');

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $viewArticle = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.view', compact('viewArticle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $updateArticle = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.update', compact('updateArticle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = Validator::make($request->all(), [
            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validatedData->fails()) {
            return redirect()->back()
                ->withErrors($validatedData)
                ->withInput();
        }
        if($request->hasFile('image')){

            
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Article'), $imageName);
            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'favourite' => $request['favourite'],
                'image' => $imageName,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'favourite' => $request['favourite'],

            ];

        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Update Article!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();

        File::delete(public_path('images/Article'.$delete->image));
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllArtical")->with('message', 'Successfully Delete Article!!');

    }




    // this function will cut the string by how many words you want
    private  function word_teaser($string, $count = 20)
    {
        $original_string = $string;
        $words = explode(' ', $original_string);

        if (count($words) > $count) {
            $words = array_slice($words, 0, $count);
            $string = implode(' ', $words);
        }
        return $string;
    }

    public function getAllArticle(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $articles = DB::table('allpost')->where('type', '=', 'Article')->get();
        //dd($articles);
        $data = array();
            foreach ($articles as $allArticle) {


               if($allArticle->status == '1'){
                   $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="'. route('admin.unpublished',[$allArticle->id]) . '"><i class="fa fa-pause"></i></a>';
               }else{
                   $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="'. route('admin.published',[$allArticle->id]) . '"><i class="fa fa-pause"></i></a>';
               }

                $data[] = [

                    $allArticle->author_name, str_limit($allArticle->title, 15), $allArticle->published_date,str_limit($allArticle->post_body,20),
                    '<img height="50" src="'.asset('ghunghur/public/images/Article').DIRECTORY_SEPARATOR.$allArticle->image.'">',
                    '<a class="btn btn-primary btn-xs" href="'. route('admin.view',[$allArticle->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="'. route('admin.update',[$allArticle->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this article ?\');"  href="'. route('admin.destroy',[$allArticle->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                    .' '.$unpublished,


                ];

            }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function unpublished($id,Request $request){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllArtical")->with('message', 'Successfully Unpublished!!');

    }

    public function published($id,Request $request){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllArtical")->with('message', 'Successfully Published!!');

    }





}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class RhymeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddRhyme()
    {

        return view('admin.AddRhyme');
    }

    public function StoreRhyme(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddRhyme')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Rhyme'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddRhyme')->with('message', 'Successfully Published');

    }


    public function AllRhyme()
    {
        $AllRhyme = DB::table('allpost')->where('type', '=', 'Rhyme')->get();
        return view('admin.AllRhyme', compact('AllRhyme'));
    }


    public function getAllRhyme(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $dataList = DB::table('allpost')->where('type', '=', 'Rhyme')->get();
        $data = array();
        foreach ($dataList as $allData) {


            if ($allData->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedRhyme', [$allData->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="published"  href="' . route('admin.publishedRhyme', [$allData->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allData->author_name, str_limit($allData->title, 15), $allData->published_date, str_limit($allData->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Rhyme') . DIRECTORY_SEPARATOR . $allData->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewRhyme', [$allData->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editRhyme', [$allData->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this?\');"  href="' . route('admin.destroyRhyme', [$allData->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroyRhyme($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();
        $path = "ghunghur/public/images/Rhyme/$delete->image";
        //dd($path);
        if(is_file($path)){
            unlink($path);
        }
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllRhyme")->with('message', 'Successfully Delete !!');

    }

    public function editRhyme($id)
    {
        //
        $updateRhyme = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateRhyme', compact('updateRhyme'));
    }

    public function updateRhyme(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Rhyme'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Update!!');
    }


    public function viewRhyme($id){

        $viewRhyme = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewRhyme', compact('viewRhyme'));
    }


    public function publishedRhyme($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllRhyme")->with('message', 'Successfully Published!!');
    }


    public function unpublishedRhyme($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllRhyme")->with('message', 'Successfully Unpublished!!');

    }
}

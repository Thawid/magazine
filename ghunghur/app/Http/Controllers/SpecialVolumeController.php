<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class SpecialVolumeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }
    public function AddSpecialVolume()
    {

        return view('admin.AddSpecialVolume');
    }

    public function StoreSpecialVolume(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'filename.*' => 'mimes:doc,pdf,docx',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddSpecialVolume')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/SpecialVolume'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];

        //dd($input);

        DB::table('allpost')->insert($input);
        $id = DB::getPdo()->lastInsertId();
        //dd($id);
        if($request->hasfile('filename'))
        {
            $fileInput= array();
            foreach($request->file('filename') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/files/', $name);
                $fileInput[] = [

                    'post_id'=>$id,
                    'filename'=>$name,
                ];


            }
            //$implodeData = \GuzzleHttp\json_encode($fileInput);
            DB::table('files')->insert($fileInput);
        }


       return redirect('admin/AddSpecialVolume')->with('message', 'Successfully Published');

    }


    public function AllSpecialVolume()
    {
        $specialVolume = DB::table('allpost')->where('type', '=', 'SpecialVolume')->get();
        return view('admin.AllSpecialVolume', compact('specialVolume'));
    }


    public function getSpecialVolume(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $allData = DB::table('allpost')->where('type', '=', 'SpecialVolume')->get();
        $data = array();
         //dd($allData);
        foreach ($allData as $dataList) {

            //dd(strips_tags(str_limit($dataList->post_body, 20)));

            // dd(strip_tags($dataList->post_body));

            if ($dataList->status == '1') {

                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedSpecialVolume', [$dataList->id]) . '"><i class="fa fa-pause"></i></a>';

            } else {

                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedSpecialVolume', [$dataList->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $dataList->author_name, str_limit($dataList->title, 15), $dataList->published_date, str_limit($dataList->post_body,50),
                '<img height="50" src="' . asset('ghunghur/public/images/SpecialVolume') . DIRECTORY_SEPARATOR . $dataList->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewSpecialVolume', [$dataList->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editSpecialVolume', [$dataList->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this data ?\');"  href="' . route('admin.destroySpecialVolume', [$dataList->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroySpecialVolume($id)
    {
        $delete = DB::table('allpost')->where('id', '=', $id)->first();
        $file_path = "ghunghur/public/images/SpecialVolume/$delete->image";
       //dd($file_path);
       if(is_file($file_path)){
           unlink($file_path);
       }
       DB::table('allpost')->where('id', '=', $id)->delete();

       return redirect("admin/AllSpecialVolume")->with('message', 'Successfully Delete!!');

    }


    public function editSpecialVolume($id)
    {
        //
        $updateSpecialVolume = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateSpecialVolume', compact('updateSpecialVolume'));
    }

    public function updateSpecialVolume(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/SpecialVolume'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        if($request->hasfile('filename'))
        {
            $fileInput= array();
            foreach($request->file('filename') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/files/', $name);
                $fileInput[] = [

                    'post_id'=>$id,
                    'filename'=>$name,
                ];


            }
            DB::table('files')->where('post_id','=',$id)->insert($fileInput);

        }


        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Updated!!');
    }



    public function viewSpecialVolume($id){

        $viewSpecialVolume = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewSpecialVolume', compact('viewSpecialVolume'));
    }



    public function publishedSpecialVolume($id) {

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllSpecialVolume")->with('message', 'Successfully Published!!');
    }


    public function unpublishedSpecialVolume($id) {

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllSpecialVolume")->with('message', 'Successfully Unpublished!!');

    }



    public function SpecialVolumeList(){

        $allFiles = DB::table('allpost')->join('files', 'allpost.id','=','files.post_id')->where('allpost.type', '=','SpecialVolume')->get();
       // dd($allFiles);
        return view('admin.SpecialVolumeList', compact('allFiles'));
    }

    public function getAllSpecialVolume(Request $request){

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $allFile = DB::table('allpost')->join('files', 'allpost.id','=','files.post_id')->where('allpost.type', '=','SpecialVolume')->get();
        $data = array();
        // dd($allFile);
        foreach ($allFile as $fileList) {





            $data[] = [

                $fileList->author_name, str_limit($fileList->title, 15), $fileList->published_date,
                '<a  href="' . secure_url('ghunghur/public/files') . DIRECTORY_SEPARATOR . $fileList->filename . '">'.$fileList->filename.'</a>',
                    '<a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this data ?\');"  href="' . route('admin.destroySpecialFile', [$fileList->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>',


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroySpecialFile($id)
    {
        $delete = DB::table('files')->where('id', '=', $id)->first();
        $file_path = "ghunghur/public/files/$delete->filename";
        //dd($file_path);
        if(is_file($file_path)){
            unlink($file_path);
        }
        DB::table('files')->where('id', '=', $id)->delete();
        return redirect("admin/SpecialVolumeList")->with('message', 'Successfully Delete!!');

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class MovieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddMovie()
    {

        return view('admin.AddMovie');
    }

    public function StoreMovie(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddMovie')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Movie'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddMovie')->with('message', 'Movi Successfully Published');

    }


    public function AllMovies()
    {
        $movie = DB::table('allpost')->where('type', '=', 'Movie')->get();
        return view('admin.AllMovies', compact('movie'));
    }


    public function getAllMovies(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $movie = DB::table('allpost')->where('type', '=', 'Movie')->get();
        $data = array();
        foreach ($movie as $allmovie) {


            if ($allmovie->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedMovie', [$allmovie->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedMovie', [$allmovie->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allmovie->author_name, str_limit($allmovie->title, 15), $allmovie->published_date, str_limit($allmovie->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Movie') . DIRECTORY_SEPARATOR . $allmovie->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewMovie', [$allmovie->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editMovie', [$allmovie->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this data ?\');"  href="' . route('admin.destroyMovie', [$allmovie->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }



    public function destroyMovie($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();

        File::delete(public_path('images/Movie' . $delete->image));
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllMovies")->with('message', 'Successfully Delete Movie!!');

    }


    public function editMovie($id)
    {
        //
        $updateMovie = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateMovie', compact('updateMovie'));
    }

    public function updateMovie(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Movie'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Updated!!');
    }


    public function viewMovie($id){

        $viewMovie = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewMovie', compact('viewMovie'));
    }


    public function publishedMovie($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllMovies")->with('message', 'Successfully Published!!');
    }


    public function unpublishedMovie($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllMovies")->with('message', 'Successfully Unpublished!!');

    }

}

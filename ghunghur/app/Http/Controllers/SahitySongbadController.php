<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class SahitySongbadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }
    public function AddSahitySongbad()
    {

        return view('admin.AddSahitySongbad');
    }

    public function StoreSahitySongbad(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddSahitySongbad')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/SahitySongbad'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddSahitySongbad')->with('message', 'Successfully Published');

    }


    public function AllSahitySongbad()
    {
        $SahitySongbad = DB::table('allpost')->where('type', '=', 'SahitySongbad')->get();
        return view('admin.AllSahitySongbad', compact('SahitySongbad'));
    }


    public function getAllSahitySongbad(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $allData = DB::table('allpost')->where('type', '=', 'SahitySongbad')->get();
        $data = array();
        // dd($allData);
        foreach ($allData as $dataList) {

            //dd(strips_tags(str_limit($dataList->post_body, 20)));

            // dd(strip_tags($dataList->post_body));

            if ($dataList->status == '1') {

                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedSahitySongbad', [$dataList->id]) . '"><i class="fa fa-pause"></i></a>';

            } else {

                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedSahitySongbad', [$dataList->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $dataList->author_name, str_limit($dataList->title, 15), $dataList->published_date, str_limit($dataList->post_body,50),
                '<img height="50" src="' . asset('ghunghur/public/images/SahitySongbad') . DIRECTORY_SEPARATOR . $dataList->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewSahitySongbad', [$dataList->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editSahitySongbad', [$dataList->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this data ?\');"  href="' . route('admin.destroySahitySongbad', [$dataList->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroySahitySongbad($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();

        File::delete(public_path('images/SahitySongbad' . $delete->image));
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllSahitySongbad")->with('message', 'Successfully Delete!!');

    }


    public function editSahitySongbad($id)
    {
        //
        $updateSahitySongbad = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateSahitySongbad', compact('updateSahitySongbad'));
    }

    public function updateSahitySongbad(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/SahitySongbad'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Updated!!');
    }

    public function viewSahitySongbad($id){

        $viewSahitySongbad = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewSahitySongbad', compact('viewSahitySongbad'));
    }

    public function publishedSahitySongbad($id) {

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllSahitySongbad")->with('message', 'Successfully Published!!');
    }


    public function unpublishedSahitySongbad($id) {

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllSahitySongbad")->with('message', 'Successfully Unpublished!!');

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class BooksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddBooks()
    {

        return view('admin.AddBooks');
    }

    public function StoreBooks(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddBooks')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Books'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddBooks')->with('message', 'Successfully Published');

    }


    public function AllBooks()
    {
        $AllBooks = DB::table('allpost')->where('type', '=', 'Books')->get();
        return view('admin.AllBooks', compact('AllBooks'));
    }


    public function getAllBooks(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $dataList = DB::table('allpost')->where('type', '=', 'Books')->get();
        $data = array();
        foreach ($dataList as $allData) {


            if ($allData->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedBooks', [$allData->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="published"  href="' . route('admin.publishedBooks', [$allData->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allData->author_name, str_limit($allData->title, 15), $allData->published_date, str_limit($allData->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Books') . DIRECTORY_SEPARATOR . $allData->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewBooks', [$allData->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editBooks', [$allData->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this?\');"  href="' . route('admin.destroyBooks', [$allData->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroyBooks($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();
        $path = "ghunghur/public/images/Books/$delete->image";
        //dd($path);
        if(is_file($path)){
            unlink($path);
        }
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllBooks")->with('message', 'Successfully Delete !!');

    }

    public function editBooks($id)
    {
        //
        $updateBooks = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateBooks', compact('updateBooks'));
    }

    public function updateBooks(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Books'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Update!!');
    }


    public function viewBooks($id){

        $viewBooks = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewBooks', compact('viewBooks'));
    }


    public function publishedBooks($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllBooks")->with('message', 'Successfully Published!!');
    }


    public function unpublishedBooks($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllBooks")->with('message', 'Successfully Unpublished!!');

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ImageLinkController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }


    public function addImage()

    {
        //dd('hello world');
        return view('admin.addImage');
    }


    public function storeImage(Request $request)
    {

        $validateData = Validator::make($request->all(), [


            'filename' => 'required',

        ]);

        if ($validateData->fails()) {

            return redirect('admin/addImage')
                ->withErrors($validateData)
                ->withInput();

        }


        if($request->hasFile('filename'))
        {
            $fileInput= array();
            foreach($request->file('filename') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/images/Bigapon', $name);
                $fileInput[] = [


                    'filename'=>$name,

                ];


            }
            //$implodeData = \GuzzleHttp\json_encode($fileInput);
            DB::table('image_link')->insert($fileInput);
        }


        return redirect('admin/addImage')->with('message', 'Successfully Published');
    }

    public function viewImageLink()
    {
        $viewImageLink = DB::table('image_link')->get();
        //dd($viewAdvertising);
        //return view('admin.viewAdvertising');
        return view('admin.viewImageLink', compact('viewImageLink'));
    }

    public function getAllLink(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $dataList = DB::table('image_link')->get();
        $data = array();
        foreach ($dataList as $allData) {
            $path = "https://ghunghur.com/ghunghur/public/images/Bigapon/$allData->filename";
            // dd($path);
            $data[] = [

                $path,
                '<img height="50"  src="'.secure_url($path) .'">',
                '<a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this?\');"  href="' . route('admin.linkDestroy', [$allData->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                ,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function linkDestroy($id)

    {

        $delete = DB::table('image_link')->where('id', '=', $id)->first();
        $path = "ghunghur/public/images/Bigapon/$delete->filename";
        //dd($path);
        if(is_file($path)){
            unlink($path);
        }
        DB::table('image_link')->where('id', '=', $id)->delete();
        return redirect("admin/viewImageLink")->with('message', 'Successfully Delete !!');


    }
}

<?php

namespace App\Http\Controllers;

use App\HomePainting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomePaintingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }


    public function addPaintingImage()

    {
        //dd('hello world');
        return view('admin.addPaintingImage');
    }


    public function storePainting(Request $request)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images/Painting'),$imageName);

        $imageUpload = new HomePainting();
        $imageUpload->filename = $imageName;
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
    }

    public function viewHomePainting()
    {
        $viewHomePainting = DB::table('home_paintings')->get();
        return view('admin.viewHomePainting', compact('viewHomePainting'));
    }
    public function getHomePainting(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $dataList = DB::table('home_paintings')->get();
        $data = array();
        foreach ($dataList as $allData) {

            $data[] = [

                $allData->created_at,
                '<img height="50" src="' . asset('ghunghur/public/images/Painting') . DIRECTORY_SEPARATOR . $allData->filename . '">',
                '<a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this?\');"  href="' . route('admin.paintingDestroy', [$allData->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                ,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function paintingDestroy($id)

    {

        $delete = DB::table('home_paintings')->where('id', '=', $id)->first();
        $path = "ghunghur/public/images/Painting/$delete->filename";
        //dd($path);
        if(is_file($path)){
            unlink($path);
        }
        DB::table('home_paintings')->where('id', '=', $id)->delete();
        return redirect("admin/viewHomePainting")->with('message', 'Successfully Delete !!');


    }
}

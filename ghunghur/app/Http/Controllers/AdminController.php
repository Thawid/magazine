<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }


    public function shoProfile(){
        $profile = DB::table('admins')->get();

        return view('admin.AdminUser', compact('profile'));

    }

    public function editProfile($id)
    {
        //
        $updatePass = DB::table('admins')->where('id', '=', $id)->first();
        //dd($updatePass);
        return view('admin.updatePass', compact('updatePass'));
    }

    public function updateProfile(Request $request, $id)
    {

        $validatedData =  Validator::make($request->all(),[
            'name' => 'required|max:40',
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        if ($validatedData->fails()) {
            return redirect('admin/showProfile')
                ->withErrors($validatedData)
                ->withInput();
        }

        $inputs=[
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ];

        DB::table('admins')->where('id', '=', $id)->update($inputs);
        //return redirect("admin/showProfile");
        return redirect()->back()->with('message', 'Successfully Update!!');


    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ArchiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }
    public function AddArchive()
    {

        return view('admin.AddArchive');
    }

    public function StoreArchive(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'filename.*' => 'mimes:doc,pdf,docx',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddArchive')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Archive'), $imageName);
        $input = [

            'title' => $request['title'],
            'number' => $request['number'],
            'published_date' => $request['published_date'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];

        //dd($input);

        DB::table('allpost')->insert($input);
        $id = DB::getPdo()->lastInsertId();
        //dd($id);
        if($request->hasfile('filename'))
        {
            $fileInput= array();
            foreach($request->file('filename') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/Archive/', $name);
                $fileInput[] = [

                    'post_id'=>$id,
                    'filename'=>$name,
                ];


            }
            //$implodeData = \GuzzleHttp\json_encode($fileInput);
            DB::table('files')->insert($fileInput);
        }


        return redirect('admin/AddArchive')->with('message', 'Successfully Published');

    }


    public function AllArchive()
    {
        $allArchive = DB::table('allpost')->join('files', 'allpost.id','=','files.post_id')->where('allpost.type', '=','Archive')->get();

        return view('admin.AllArchive', compact('allArchive'));
    }


    public function getAllArchive(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $allData = DB::table('allpost')->join('files', 'allpost.id','=','files.post_id')->where('allpost.type', '=','Archive')->get();

        $data = array();
        //dd($allData);
        foreach ($allData as $dataList) {

            //dd(strips_tags(str_limit($dataList->post_body, 20)));

            // dd(strip_tags($dataList->post_body));

            if ($dataList->status == '1') {

                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedArchive', [$dataList->post_id]) . '"><i class="fa fa-pause"></i></a>';

            } else {

                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedSArchive', [$dataList->post_id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                str_limit($dataList->title, 15), $dataList->published_date,
                '<img height="50" src="' . asset('ghunghur/public/images/Archive') . DIRECTORY_SEPARATOR . $dataList->image . '">',
                '<a  href="' . secure_url('ghunghur/public/Archive') . DIRECTORY_SEPARATOR . $dataList->filename . '">'.$dataList->filename.'</a>',
                '<a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this data ?\');"  href="' . route('admin.destroyArchive', [$dataList->post_id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroyArchive($id)
    {
        $delete = DB::table('allpost')->join('files', 'allpost.id','=','files.post_id')->where('allpost.type', '=','Archive')->where('allpost.id','=',$id)->first();
        //dd($delete);
        $img_path = "ghunghur/public/images/Archive/$delete->image";

        $file_path = "ghunghur/public/Archive/$delete->filename";
        //dd($file_path);
        if(is_file($img_path)){

            unlink($img_path);
        }
        if(is_file($file_path)){

            unlink($file_path);
        }
        DB::table('allpost')->where('id', '=', $id)->delete();
        DB::table('files')->where('post_id', '=', $id)->delete();

        return redirect("admin/AllArchive")->with('message', 'Successfully Delete!!');

    }

    public function publishedSArchive($id) {

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllArchive")->with('message', 'Successfully Published!!');
    }


    public function unpublishedArchive($id) {

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllArchive")->with('message', 'Successfully Unpublished!!');

    }

}

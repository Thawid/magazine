<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class DramaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddDrama()
    {

        return view('admin.AddDrama');
    }

    public function StoreDrama(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddMovie')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Drama'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddDrama')->with('message', 'Drama Successfully Published');

    }


    public function AllDrama()
    {
        $drama = DB::table('allpost')->where('type', '=', 'Drama')->get();
        return view('admin.AllDrama', compact('drama'));
    }


    public function getAllDrama(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $drama = DB::table('allpost')->where('type', '=', 'Drama')->get();
        $data = array();
        foreach ($drama as $allDrama) {


            if ($allDrama->status == '1') {

                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedDrama', [$allDrama->id]) . '"><i class="fa fa-pause"></i></a>';

            } else {

                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedDrama', [$allDrama->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allDrama->author_name, str_limit($allDrama->title, 15), $allDrama->published_date, str_limit($allDrama->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Drama') . DIRECTORY_SEPARATOR . $allDrama->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewDrama', [$allDrama->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editDrama', [$allDrama->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this data ?\');"  href="' . route('admin.destroyDrama', [$allDrama->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }



    public function destroyDrama($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();

        File::delete(public_path('images/Drama' . $delete->image));
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllDrama")->with('message', 'Successfully Delete Drama!!');

    }


    public function editDrama($id)
    {
        //
        $updateDrama = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateDrama', compact('updateDrama'));
    }

    public function updateDrama(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Drama'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Updated!!');
    }

    public function viewDrama($id){

        $viewDrama = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewDrama', compact('viewDrama'));
    }


    public function publishedDrama($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllDrama")->with('message', 'Successfully Published!!');
    }


    public function unpublishedDrama($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllDrama")->with('message', 'Successfully Unpublished!!');

    }
}

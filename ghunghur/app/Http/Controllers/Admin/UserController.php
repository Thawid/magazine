<?php

namespace App\Http\Controllers\Admin;
use  App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Hash;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;



class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::latest()->paginate(5);
        return view('admin.users.all', compact('users'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validatedData = Validator::make($request->all(), [
            'name' => 'required|unique:users|max:40',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6'
        ]);
        if ($validatedData->fails()) {
            return redirect('users/create')
                ->withErrors($validatedData)
                ->withInput();
        }
        $inputs = [
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ];
        if (User::create($inputs)) {

            //return  redirect('users');
            // return redirect("users");
            $userName = str_replace(' ', '_', $request['name']);
            $tableName = strtolower($userName);
            //dd($tableName);
            Schema::create('log_'.$tableName, function (Blueprint $table) {
                $table->increments('id');
                $table->string('adminid');
                $table->string('ip')->nullable();
                $table->string('ip_history')->nullable();
                $table->string('log_ip_add_date')->nullable();
                $table->string('del_ip')->nullable();
                $table->string('log_ip_del_date')->nullable();
                $table->string('sip_ip')->nullable();
                $table->string('sip_ip_history')->nullable();
                $table->string('sip_ip_add_date')->nullable();
                $table->string('del_sip_ip')->nullable();
                $table->string('sip_delete_date')->nullable();
                $table->integer('status')->default('0');
                $table->integer('signature')->default('0');

                $table->timestamps();
            });


            return redirect("users");

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::find($id);
        return view('users.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        return view('users.update', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = Validator::make($request->all(), [
            'name' => 'required|max:40',
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        if ($validatedData->fails()) {
            return redirect('users/create')
                ->withErrors($validatedData)
                ->withInput();
        }

        $inputs = [
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ];
        User::find($id)->update($inputs);

        return redirect("users");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::find($id)->delete();

        return redirect("users");

    }





}
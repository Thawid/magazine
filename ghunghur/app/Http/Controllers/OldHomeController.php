<?php

namespace App\Http\Controllers;

use App\UserLoginHistory;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use LiveControl\EloquentDataTable\DataTable;
use App\LogUser;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$result = DB::table('log_tanvir')->select('adminid', 'ip')->Where('adminid', '=', auth()->user()->name)->orderby('id', 'desc')->first();

        //return view('welcome2', compact('result'));

        $userName = DB::table('users')->select('name')->where('name', '=', auth()->user()->name)->first();
        //dd($userName);


        $loginDetails[] = DB::table('log_'.strtolower($userName->name))->get();


        //dd('log_'.strtolower($userName->name));
        $tableName = ('log_'.strtolower($userName->name));
        // dd($tableName);
        $ipDetails = DB::table ($tableName)->where('adminid', $userName->name)->orderBy('id', 'desc')->first();

        //dd($ipDetails);
        if($ipDetails->signature == 0){

            $clientIP = $ipDetails->ip;

            //dd($ipDetails->signature);

            return view('welcome2',['user_id'=>$userName->name,'clientIP'=>$clientIP]);

        }elseif($ipDetails->signature == 1){
            $syncClientIP = $ipDetails->ip;
            // dd($syncClientIP);
            return view('welcome2', ['user_id'=>$userName->name,'syncClientIP'=>$syncClientIP]);
        }



    }

    public function UserLoginHistory(){



        /*        $userName = DB::table('users')->select('name')->where('name', '=', auth()->user()->name)->first();
                //dd($userName);


                $loginDetails[] = DB::table('log_'.strtolower($userName->name))->get();


                //dd('log_'.strtolower($userName->name));
                $tableName = ('log_'.strtolower($userName->name));
                // dd($tableName);
                $ipDetails = DB::table ($tableName)->where('adminid', $userName->name)->orderBy('id', 'desc')->get();

               //dd($ipDetails);

                return view('layouts.loginDetails', compact('ipDetails'));*/

        return view('layouts.loginDetails');


    }

    public  function  homeLogDetails(){
        // user last login ip history

        $userName = DB::table('users')->select('name')->where('name', '=', auth()->user()->name)->first();
        //dd($userName);

        foreach ($userName as $name) {

            $logHistory = new UserLoginHistory();

            $logHistory->set_table('log_' .strtolower($name));
            //dd($logHistory);

        }
        $dataTable = new DataTable(
            $logHistory->where('adminid', $name)->where('signature', '=', 1)->take(5)->orderBy('id', 'DESC'),
            ['id','ip', 'log_ip_add_date']
        );

        $dataTable->setFormatRowFunction(function ($logHistory) {
            return [
                $logHistory->id,
                $logHistory->ip,
                $logHistory->log_ip_add_date,

            ];
        });

        return $dataTable->make();
    }

    public  function getLogDetails(){

        $userName = DB::table('users')->select('name')->where('name', '=', auth()->user()->name)->first();
        //dd($userName);

        foreach ($userName as $name) {

            $logHistory = new UserLoginHistory();

            $logHistory->set_table('log_' .strtolower($name));
            //dd($logHistory);

        }
        $dataTable = new DataTable(
            $logHistory->where('adminid', $name)->where('signature', '=', 1)->orderBy('id', 'DESC'),
            ['id', 'ip', 'log_ip_add_date']
        );

        $dataTable->setFormatRowFunction(function ($logHistory) {
            return [
                $logHistory->id,
                $logHistory->ip,
                $logHistory->log_ip_add_date,

                '<a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this IP?\');"  href="'. route('layouts.deleteIP',[$logHistory->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
            ];
        });

        return $dataTable->make();
    }

    public function deleteIP($id)
    {
        $userName = DB::table('users')->select('name')->where('name', '=', auth()->user()->name)->first();
        //dd($userName);


        $loginDetails[] = DB::table('log_'.strtolower($userName->name))->get();


        //dd('log_'.strtolower($userName->name));
        $tableName = ('log_'.strtolower($userName->name));

        //DB::table($tableName)->where('id', $id)->delete();

        $ipDetails = DB::table ($tableName)->where('id', $id)->first();

        //dd($ipDetails->ip,$ipDetails->log_ip_add_date);
        $delIP = $ipDetails->ip;
        //$delIPAddDate = $ipDetails->log_ip_add_date;


        DB::table($tableName)
            ->where('id', $id)
            ->update([
                'ip' => 0,
                'log_ip_add_date' => 0,
                'del_ip' => $delIP,
                'log_ip_del_date' => Carbon::now()->toDateTimeString(),
                'signature' => 2,
            ]);

        return redirect("loginDetails");

    }


    public function getIPStatus(Request $request){

        $userName = $request->input('adminid');

        $log = new LogUser();

        $log->set_table('log_'.$userName);
        $ipDetails = $log->where('adminid', $userName)->where('ip', '!=', 0)->orderBy('id', 'desc')->first();
        //dd($ipDetails);
        if($ipDetails->signature == 0){

            return '<p> Your IP Is : '.$ipDetails->ip.' , Please Wait For Sync..</p>';
        }else{
            return '<p> Your IP Is : '.$ipDetails->ip.' , is successfully Synced</p>';
        }
    }

    public function UserAllSIP(){


        return view('layouts.UserAllSIP');


    }

    public  function UserAllSIPip(){


        $userName = DB::table('users')->select('name')->where('name', '=', auth()->user()->name)->first();
        //dd($userName);

        foreach ($userName as $name) {

            $sipIP = new UserLoginHistory();

            $sipIP->set_table('log_' .strtolower($name));
            //dd($sipIP);

        }
        $dataTable = new DataTable(
            $sipIP->where('adminid', $name)->where('status', '=', 5),
            ['sip_ip', 'sip_ip_add_date','id']
        );

        $dataTable->setFormatRowFunction(function ($allSIPip) {
            return [
                $allSIPip->sip_ip,
                $allSIPip->sip_ip_add_date,

                '<a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this IP?\');"  href="'. route('layouts.deleteSIPIP',[$allSIPip->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
            ];
        });

        return $dataTable->make();


    }


    public function deleteSIPIP($id)
    {
        $userName = DB::table('users')->select('name')->where('name', '=', auth()->user()->name)->first();
        //dd($userName);


        $loginDetails[] = DB::table('log_'.strtolower($userName->name))->get();


        //dd('log_'.strtolower($userName->name));
        $tableName = ('log_'.strtolower($userName->name));

        //DB::table($tableName)->where('id', $id)->delete();

        $ipDetails = DB::table ($tableName)->where('id', $id)->first();

        //dd($ipDetails->ip,$ipDetails->log_ip_add_date);
        $delIP = $ipDetails->sip_ip;
        //$delIPAddDate = $ipDetails->log_ip_add_date;


        DB::table($tableName)
            ->where('id', $id)
            ->update([
                'sip_ip' => 0,
                'sip_ip_add_date' => 0,
                'del_sip_ip' => $delIP,
                'sip_delete_date' => Carbon::now()->toDateTimeString(),
                'status' => 6,
            ]);

        return redirect("UserAllSIP");

    }



}

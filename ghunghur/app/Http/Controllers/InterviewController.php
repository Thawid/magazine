<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class InterviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddInterview()
    {

        return view('admin.AddInterview');
    }

    public function StoreInterview(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddInterview')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Interview'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddInterview')->with('message', 'Interview Successfully Published');

    }


    public function AllInterview()
    {
        $interviews = DB::table('allpost')->where('type', '=', 'Interview')->get();
        return view('admin.AllInterview', compact('interviews'));
    }


    public function getAllInterview(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $interview = DB::table('allpost')->where('type', '=', 'Interview')->get();
        $data = array();
        foreach ($interview as $allInterview) {


            if ($allInterview->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedInterview', [$allInterview->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedInterview', [$allInterview->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allInterview->author_name, str_limit($allInterview->title, 15), $allInterview->published_date, str_limit($allInterview->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Interview') . DIRECTORY_SEPARATOR . $allInterview->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewInterview', [$allInterview->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editInterview', [$allInterview->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this interview ?\');"  href="' . route('admin.destroyInterview', [$allInterview->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroyInterview($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();

        File::delete(public_path('images/Interview' . $delete->image));
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllInterview")->with('message', 'Successfully Delete Interview!!');

    }


    public function editInterview($id)
    {
        //
        $updateInterview = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateInterview', compact('updateInterview'));
    }

    public function updateInterview(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Interview'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Updated!!');
    }

    public function viewInterview($id){

        $viewInterview = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewInterview', compact('viewInterview'));
    }


    public function publishedInterview($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllInterview")->with('message', 'Successfully Published!!');
    }


    public function unpublishedInterview($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllInterview")->with('message', 'Successfully Unpublished!!');

    }



}

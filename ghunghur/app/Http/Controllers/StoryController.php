<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class StoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddStory()
    {

        return view('admin.AddStory');
    }

    public function StoreStory(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:shortstory,title',
            'post_body' => 'required',
            'image' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddStory')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Story'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddStory')->with('message', 'Short Story Successfully Published');

    }

    public function AllStory()
    {
        $short_strory = DB::table('allpost')->where('type', '=', 'Story')->get();
        return view('admin.AllStory', compact('short_strory'));
    }


    public function getAllStory(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $short_story = DB::table('allpost')->where('type', '=', 'Story')->get();
        $data = array();
        foreach ($short_story as $allStory) {


            if ($allStory->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedStory', [$allStory->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedStory', [$allStory->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allStory->author_name, str_limit($allStory->title, 15), $allStory->published_date, str_limit($allStory->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Story') . DIRECTORY_SEPARATOR . $allStory->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewStory', [$allStory->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editStory', [$allStory->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this short story ?\');"  href="' . route('admin.destroyStory', [$allStory->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }

    public function destroyStory($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();

        File::delete(public_path('images/Story' . $delete->image));
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllStory")->with('message', 'Successfully Delete Short Story!!');

    }

    public function editStory($id)
    {
        //
        $updateShortstory = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateStory', compact('updateShortstory'));
    }

    public function updateStory(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Story'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Update Short Story!!');
    }

    public function viewStory($id){

        $viewStory = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewStory', compact('viewStory'));
    }


    public function publishedStory($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllStory")->with('message', 'Successfully Published!!');
    }


    public function unpublishedStory($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllStory")->with('message', 'Successfully Unpublished!!');

    }




}

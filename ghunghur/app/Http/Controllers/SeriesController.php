<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class SeriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddSeries()
    {

        return view('admin.AddSeries');
    }

    public function StoreSeries(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddSeries')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Series'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddSeries')->with('message', 'Series Successfully Published');

    }


    public function AllSeries()
    {
        $series = DB::table('allpost')->where('type', '=', 'Series')->get();
        //dd($series);
        return view('admin.AllSeries', compact('series'));
    }


    public function getAllSeries(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $series = DB::table('allpost')->where('type', '=', 'Series')->get();
        $data = array();
        foreach ($series as $allSeries) {


            if ($allSeries->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedSeries', [$allSeries->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedSeries', [$allSeries->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allSeries->author_name, str_limit($allSeries->title, 15), $allSeries->published_date, str_limit($allSeries->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Series') . DIRECTORY_SEPARATOR . $allSeries->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewSeries', [$allSeries->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editSeries', [$allSeries->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this series ?\');"  href="' . route('admin.destroySeries', [$allSeries->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }

    public function destroySeries($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();

        File::delete(public_path('images/Series' . $delete->image));
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllSeries")->with('message', 'Successfully Delete Series!!');

    }

    public function editSeries($id)
    {
        //
        $updateSeries = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateSeries', compact('updateSeries'));
    }

    public function updateSeries(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Series'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Updated!!');
    }


    public function viewSeries($id){

        $viewSeries = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewSeries', compact('viewSeries'));
    }

    public function publishedSeries($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllSeries")->with('message', 'Successfully Published!!');
    }


    public function unpublishedSeries($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllSeries")->with('message', 'Successfully Unpublished!!');

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class RehearseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddRehearse()
    {

        return view('admin.AddRehearse');
    }

    public function StoreAddRehearse(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddRehearse')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Rehearse'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddRehearse')->with('message', 'Successfully Published');

    }


    public function AllRehearse()
    {
        $AllRehearse = DB::table('allpost')->where('type', '=', 'Rehearse')->get();
        return view('admin.AllRehearse', compact('AllRehearse'));
    }


    public function getAllRehearse(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $dataList = DB::table('allpost')->where('type', '=', 'Rehearse')->get();
        $data = array();
        foreach ($dataList as $allData) {


            if ($allData->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedRehearse', [$allData->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="publishedRehearse"  href="' . route('admin.publishedRehearse', [$allData->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allData->author_name, str_limit($allData->title, 15), $allData->published_date, str_limit($allData->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Rehearse') . DIRECTORY_SEPARATOR . $allData->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewRehearse', [$allData->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editRehearse', [$allData->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this?\');"  href="' . route('admin.destroyRehearse', [$allData->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroyRehearse($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();
        $path = "ghunghur/public/images/Rehearse/$delete->image";
        //dd($path);
        if(is_file($path)){
            unlink($path);
        }
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllRehearse")->with('message', 'Successfully Delete !!');

    }

    public function editRehearse($id)
    {
        //
        $updateRehearse = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateRehearse', compact('updateRehearse'));
    }

    public function updateRehearse(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Rehearse'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Update!!');
    }


    public function viewRehearse($id){

        $viewRehearse = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewRehearse', compact('viewRehearse'));
    }


    public function publishedRehearse($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllRehearse")->with('message', 'Successfully Published!!');
    }


    public function unpublishedRehearse($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllRehearse")->with('message', 'Successfully Unpublished!!');

    }
}

<?php

namespace App\Http\Controllers\WebController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RehearseController extends Controller
{
    public function rehearse(){

        $rehearse = DB::table('allpost')->where(
            [
                ['type', '=', 'Rehearse'],
                ['status','=','1'],
            ]
        )->orderBy('id','desc')->paginate(9);

         $recentPost = DB::table('allpost')->where(
            [
                 ['type','!=','Archive'],
                 ['type','!=','BookHouse'],
                 ['status', '=', 1]
            ]
        )
        ->orderBy('id', 'desc')->limit('5')->get();

        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
                ['status','=', 1]
            ]
        )->orderBy('id','desc')->limit('5')->get();


        return view('web.pages.rehearse',[

            'rehearse'=>$rehearse,
            'recentPost' => $recentPost,
            'favouriteArticle' => $favouriteArticle,



        ]);
    }

    public function showRehearse($id, Request $request){

        $postDetails = DB::table('allpost')->where('id','=', $id)->first();

        $readMore = DB::table('allpost')->where('type','=','Rehearse')->orderBy('id','desc')->limit('3')->get();

        $recentPost = DB::table('allpost')->orderBy('id', 'desc')->limit('5')->get();

        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
            ]
        )->orderBy('id','desc')->limit('4')->get();
        // dd($articaleDetails);
        return view('web.pages.showRehearse',[

            'postDetails'=>$postDetails,
            'readMore'=>$readMore,
            'recentPost' => $recentPost,
            'favouriteArticle' => $favouriteArticle,
        ]);
    }
}

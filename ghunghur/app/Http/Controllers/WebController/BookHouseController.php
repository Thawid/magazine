<?php

namespace App\Http\Controllers\WebController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BookHouseController extends Controller
{
    public function bookHouse(){

        $bookHouse = DB::table('allpost')->where(
            [
                ['type', '=', 'BookHouse'],
                ['status','=','1'],
            ]
        )->orderBy('id','desc')->paginate(9);
        //dd($bookHouse);
        
         $recentPost = DB::table('allpost')->where(
            [
                 ['type','!=','Archive'],
                 ['type','!=','BookHouse'],
                 ['status', '=', 1]
            ]
        )
        ->orderBy('id', 'desc')->limit('5')->get();

        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
                ['status','=', 1]
            ]
        )->orderBy('id','desc')->limit('5')->get();

        
       
        return view('web.pages.bookHouse',[

            'bookHouse'=>$bookHouse,
            'recentPost' => $recentPost,
            'favouriteArticle' => $favouriteArticle
            



        ]);
    }
}

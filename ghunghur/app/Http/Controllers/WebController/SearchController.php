<?php

namespace App\Http\Controllers\WebController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    public function index(Request $request)
    {


        $search = $request->input('search');

        $search_result = DB::table('allpost')->where('author_name', 'like', '%'.$search.'%')
            ->orWhere('title', 'like', '%'.$search.'%')
            ->orWhere('post_body', 'like', '%'.$search.'%')->get();

        $recentPost = DB::table('allpost')->orderBy('id', 'desc')->limit('5')->get();

        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
            ]
        )->orderBy('id','desc')->limit('4')->get();

        //dd($search_result);

        return view('web.pages.search',  [
            'search'=>$search_result,
            'recentPost' => $recentPost,
            'favouriteArticle' => $favouriteArticle,
        ]);

    }

    public function mobilesearch(Request $request)
    {


        $search = $request->input('search');

        $search_result = DB::table('allpost')->where('author_name', 'like', '%'.$search.'%')
            ->orWhere('title', 'like', '%'.$search.'%')
            ->orWhere('post_body', 'like', '%'.$search.'%')->get();

        $recentPost = DB::table('allpost')->orderBy('id', 'desc')->limit('5')->get();

        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
            ]
        )->orderBy('id','desc')->limit('4')->get();

        //dd($search_result);

        return view('web.pages.search',  [
            'search'=>$search_result,
            'recentPost' => $recentPost,
            'favouriteArticle' => $favouriteArticle,
        ]);

    }

}

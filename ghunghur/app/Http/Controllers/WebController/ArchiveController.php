<?php

namespace App\Http\Controllers\WebController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ArchiveController extends Controller
{
    public function Archive(){

        $archive = DB::table('allpost')->join('files', 'allpost.id','=','files.post_id')
            ->where(
            [
                ['allpost.type', '=', 'Archive'],
                ['allpost.status','=','1'],

            ]
        )->orderBy('allpost.id','desc')->paginate(9);
        //dd($bookHouse);
        
        $recentPost = DB::table('allpost')->where(
            [
                 ['type','!=','Archive'],
                 ['type','!=','BookHouse'],
                 ['status', '=', 1]
            ]
        )
        ->orderBy('id', 'desc')->limit('5')->get();

        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
                ['status','=', 1]
            ]
        )->orderBy('id','desc')->limit('5')->get();



        return view('web.pages.Archive',[

            'archive'=>$archive,
            'recentPost' => $recentPost,
            'favouriteArticle' => $favouriteArticle,



        ]);
    }
}

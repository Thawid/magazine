<?php

namespace App\Http\Controllers\WebController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SeriesController extends Controller
{
    public function allSeries(){

        $allSeries = DB::table('allpost')->where(
            [
                ['type', '=', 'Series'],
                ['status','=','1'],
            ]
        )->orderBy('id','desc')->paginate(9);

         $recentPost = DB::table('allpost')->where(
            [
                 ['type','!=','Archive'],
                 ['type','!=','BookHouse'],
                 ['status', '=', 1]
            ]
        )
        ->orderBy('id', 'desc')->limit('5')->get();

        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
                ['status','=', 1]
            ]
        )->orderBy('id','desc')->limit('5')->get();


        return view('web.pages.series',[

            'allSeries'=>$allSeries,
            'recentPost' => $recentPost,
            'favouriteArticle' => $favouriteArticle,



        ]);
    }

    public function showSeries($id, Request $request){

        $seriesDetails = DB::table('allpost')->where('id','=', $id)->first();

        $readMore = DB::table('allpost')->where('type','=','Series')->orderBy('id','desc')->limit('3')->get();

        $recentPost = DB::table('allpost')->orderBy('id', 'desc')->limit('5')->get();

        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
            ]
        )->orderBy('id','desc')->limit('4')->get();
        // dd($articaleDetails);
        return view('web.pages.showSeries',[

            'seriesDetails'=>$seriesDetails,
            'readMore'=>$readMore,
            'recentPost' => $recentPost,
            'favouriteArticle' => $favouriteArticle,
        ]);
    }
}

<?php

namespace App\Http\Controllers\WebController;

use DeepCopy\f007\FooDateTimeZone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;



class HomeController extends Controller
{
    public  function home(){
        //dd('There is some issue, we are comming soon!!');
       
         $latestPost = DB::table('allpost')->where(
            [
                 ['type','!=','Archive'],
                 ['type','!=','BookHouse'],
                 ['status', '=', 1]
            ]
        )
		->orderBy('id','desc')->first();
        // dd($latestPost);

        $singleArticle = DB::table('allpost')->where('type','=', 'Article')->orderBy('id','desc')->first();
        $recentArticle = DB::table('allpost')->where(
                [ 
                     ['type', '=', 'Article'],
                     ['status', '=', 1]
                ]
            )->orderBy('id', 'desc')->limit('5')->get();

        $singleShortStory = DB::table('allpost')->where('type', '=', 'Story')->orderBy('id','desc')->first();

        $shortPost = DB::table('allpost')->where('type', '=', 'Story')->orderBy('id', 'desc')->limit('5')->get();

        $singlePoeams = DB::table('allpost')->where('type', '=', 'Poem')->orderBy('id','desc')->first();
        $Poeams = DB::table('allpost')->where('type', '=', 'Poem')->orderBy('id','desc')->limit('5')->get();

        $singleSeries = DB::table('allpost')->where('type', '=','Series')->orderBy('id','desc')->first();
        $Series = DB::table('allpost')->where('type', '=','Series')->orderBy('id','desc')->limit('5')->get();

        $recentPost = DB::table('allpost')->where(
            [
                 ['type','!=','Archive'],
                 ['type','!=','BookHouse'],
                 ['status', '=', 1]
            ]
        )
        ->orderBy('id', 'desc')->limit('5')->get();
        
        $recentText = DB::table('allpost')->where(
            [
                 ['type','!=','Archive'],
                 ['type','!=','BookHouse'],
                 ['status', '=', 1]
            ]
        )->orderBy('id', 'desc')->offset(1)->limit('3')->get();
        
        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
                ['status','=', 1]
                
            ]
        )->orderBy('id','desc')->limit('5')->get();

        $photograph = DB::table('home_photographs')->orderBy('id', 'desc')->limit('5')->get();
        $painting = DB::table('home_paintings')->orderBy('id', 'desc')->limit('5')->get();

        //dd($eng . $ban);

        return view('web.pages.home',[
            'lastPost'=>$latestPost,
            'singleArticle'=>$singleArticle,
            'recentArticle'=>$recentArticle,
            'singleShortStory' =>$singleShortStory,
            'shortPost'=>$shortPost,
            'singlePoeams' => $singlePoeams,
            'Poeams' => $Poeams,
            'singleSeries' => $singleSeries,
            'Series' => $Series,
            'recentPost' => $recentPost,
            'recentText' => $recentText,
            'favouriteArticle' => $favouriteArticle,
            'photograph' => $photograph,
            'painting' => $painting,



        ]);
    }


    public function painting(){

    }
}

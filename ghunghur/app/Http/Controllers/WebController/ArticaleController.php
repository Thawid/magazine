<?php

namespace App\Http\Controllers\WebController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class ArticaleController extends Controller
{

    public  function allArticale(){


        $allArticale = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['status','=','1'],
            ]
        )->orderBy('id','desc')->paginate(9);

         $recentPost = DB::table('allpost')->where(
            [
                 ['type','!=','Archive'],
                 ['type','!=','BookHouse'],
                 ['status', '=', 1]
            ]
        )
        ->orderBy('id', 'desc')->limit('5')->get();

        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
                ['status','=', 1]
            ]
        )->orderBy('id','desc')->limit('5')->get();



        return view('web.pages.articale',[

            'allArticale'=>$allArticale,
            'recentPost' => $recentPost,
            'favouriteArticle' => $favouriteArticle,



        ]);
    }


    public function showArticle($id, Request $request){

        $articaleDetails = DB::table('allpost')->where('id','=', $id)->first();
        $readMore = DB::table('allpost')->where('type','=','Article')->orderBy('id','desc')->limit('3')->get();
        $recentPost = DB::table('allpost')->orderBy('id', 'desc')->limit('4')->get();
        $favouriteArticle = DB::table('allpost')->where(
            [
                ['type', '=', 'Article'],
                ['favourite', '=', '1'],
            ]
        )->orderBy('id','desc')->limit('4')->get();
       // dd($articaleDetails);
        return view('web.pages.showArticle',[

            'articaleDetails'=>$articaleDetails,
            'readMore'=>$readMore,
            'recentPost' => $recentPost,
            'favouriteArticle' => $favouriteArticle,
        ]);
    }
}

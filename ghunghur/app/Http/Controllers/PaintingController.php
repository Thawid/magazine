<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PaintingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddPainting()
    {

        return view('admin.AddPainting');
    }

    public function StorePainting(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddPainting')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Painting'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddPainting')->with('message', 'Painting Successfully Published');

    }

    public function AllPainting()
    {
        $painting = DB::table('allpost')->where('type', '=', 'Painting')->get();
        return view('admin.AllPainting', compact('painting'));
    }


    public function getAllPainting(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $painting = DB::table('allpost')->where('type', '=', 'Painting')->get();
        $data = array();
        foreach ($painting as $allPainting) {


            if ($allPainting->status == '1') {

                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedPainting', [$allPainting->id]) . '"><i class="fa fa-pause"></i></a>';

            } else {

                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedPainting', [$allPainting->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allPainting->author_name, str_limit($allPainting->title, 15), $allPainting->published_date, str_limit($allPainting->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Painting') . DIRECTORY_SEPARATOR . $allPainting->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewPainting', [$allPainting->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editPainting', [$allPainting->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this data ?\');"  href="' . route('admin.destroyPainting', [$allPainting->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroyPainting($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();

        File::delete(public_path('images/Painting' . $delete->image));
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllPainting")->with('message', 'Successfully Delete Painting!!');

    }


    public function editPainting($id)
    {
        //
        $updatePainting = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updatePainting', compact('updatePainting'));
    }

    public function updatePainting(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Painting'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Updated!!');
    }

    public function viewPainting($id){

        $viewPainting = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewPainting', compact('viewPainting'));
    }

    public function publishedPainting($id) {

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllPainting")->with('message', 'Successfully Published!!');
    }


    public function unpublishedPainting($id) {

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllPainting")->with('message', 'Successfully Unpublished!!');

    }

}

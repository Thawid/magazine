<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AdvertisingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }


    public function addAdvertising()

    {
        //dd('hello world');
        return view('admin.addAdvertising');
    }


    public function storeAdvertising(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'type' => 'required'
            
        ]);

        if ($validateData->fails()) {

            return redirect('admin/addAdvertising')
                ->withErrors($validateData)
                ->withInput();

        }


        if($request->hasFile('filename'))
        {
            $fileInput= array();
            foreach($request->file('filename') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/images/Bigapon', $name);
                $fileInput[] = [


                    'filename'=>$name,
                    'type'=>$request['type']
                ];


            }
            
            
        }else{
            $fileInput[] = [


                    
                    'type'=>$request['type'],
                    'url'=>$request['url']
                ];
            
        }
        DB::table('advertising')->insert($fileInput);

        return redirect('admin/addAdvertising')->with('message', 'Successfully Published');
    }

    public function viewAdvertising()
    {
        $viewAdvertising = DB::table('advertising')->get();
        //dd($viewAdvertising);
        //return view('admin.viewAdvertising');
        return view('admin.viewAdvertising', compact('viewAdvertising'));
    }

     public function getAllAdvertising(Request $request)
     {

         $limit = $request->input('length', 10);
         $draw = $request->input('draw', 1);
         $search = $request->input('search')['value'];
         $offset = $request->input('start', 0);
         $dataList = DB::table('advertising')->get();
         
         $data = array();
         foreach ($dataList as $allData) {
             $path = "ghunghur/public/images/Bigapon/$allData->filename";
             //$video = "$allData->url";
             //dd($allData->url);
            // dd($path);
             if(isset($allData->filename) == true){
                 $data[] = [

                 
                 '<img height="50"  src="'.secure_url($path) .'">',
                 '<a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this?\');"  href="' . route('admin.advertiseDestroy', [$allData->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                 ,


             ];

             }else{
                 $data[] = [

                 
                 '<iframe height="100" width="100" src="'.secure_url($allData->url).'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
                 '<a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this?\');"  href="' . route('admin.advertiseDestroy', [$allData->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                 ,


             ];

             }
         }

         return [
             'draw' => $draw,
             'data' => $data
         ];
     }


    public function advertiseDestroy($id)

    {

        $delete = DB::table('advertising')->where('id', '=', $id)->first();
        $path = "ghunghur/public/images/Bigapon/$delete->filename";
        //dd($path);
        if(is_file($path)){
            unlink($path);
        }
        DB::table('advertising')->where('id', '=', $id)->delete();
        return redirect("admin/viewAdvertising")->with('message', 'Successfully Delete !!');


    }
}

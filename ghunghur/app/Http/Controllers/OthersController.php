<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class OthersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddOthers()
    {

        return view('admin.AddOthers');
    }

    public function StoreOthers(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:allpost,title',
            'post_body' => 'required',
            'image' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddOthers')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/Others'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'post_body' => $request['post_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddOthers')->with('message', 'Successfully Published');

    }


    public function AllOthers()
    {
        $AllOthers = DB::table('allpost')->where('type', '=', 'Rhyme')->get();
        return view('admin.AllOthers', compact('AllOthers'));
    }


    public function getAllOthers(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $dataList = DB::table('allpost')->where('type', '=', 'Others')->get();
        $data = array();
        foreach ($dataList as $allData) {


            if ($allData->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedOthers', [$allData->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="published"  href="' . route('admin.publishedOthers', [$allData->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allData->author_name, str_limit($allData->title, 15), $allData->published_date, str_limit($allData->post_body, 20),
                '<img height="50" src="' . asset('ghunghur/public/images/Others') . DIRECTORY_SEPARATOR . $allData->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewOthers', [$allData->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editOthers', [$allData->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this?\');"  href="' . route('admin.destroyOthers', [$allData->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroyOthers($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();
        $path = "ghunghur/public/images/Others/$delete->image";
        //dd($path);
        if(is_file($path)){
            unlink($path);
        }
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllOthers")->with('message', 'Successfully Delete !!');

    }

    public function editOthers($id)
    {
        //
        $updateOthers = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateOthers', compact('updateOthers'));
    }

    public function updateOthers(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'post_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/Others'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'post_body' => $request['post_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Update!!');
    }


    public function viewOthers($id){

        $viewOthers = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.viewOthers', compact('viewOthers'));
    }


    public function publishedOthers($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllOthers")->with('message', 'Successfully Published!!');
    }


    public function unpublishedOthers($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllOthers")->with('message', 'Successfully Unpublished!!');

    }
}

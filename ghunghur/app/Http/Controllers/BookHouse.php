<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class BookHouse extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddBookHouse()
    {

        return view('admin.AddBookHouse');
    }

    public function StoreBookHouse(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'image' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddBookHouse')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/BookHouse'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => Carbon::now(),
            'date' => Carbon::now(),
            'image' => $imageName,
            'type' => $request['type'],
            'status' => 1,
        ];
        //dd($input);
        DB::table('allpost')->insert($input);
        return redirect('admin/AddBookHouse')->with('message', 'Successfully Published');

    }


    public function AllBookHouse()
    {
        $AllBookHouse = DB::table('allpost')->where('type', '=', 'BookHouse')->get();
        return view('admin.AllBookHouse', compact('AllBookHouse'));
    }


    public function getAllBookHouse(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $dataList = DB::table('allpost')->where('type', '=', 'BookHouse')->get();
        $data = array();
        foreach ($dataList as $allData) {


            if ($allData->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedBookhouse', [$allData->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="published"  href="' . route('admin.publishedBookHouse', [$allData->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allData->author_name, str_limit($allData->title, 15),
                '<img height="50" src="' . asset('ghunghur/public/images/BookHouse') . DIRECTORY_SEPARATOR . $allData->image . '">',

                    '<a class="btn btn-success btn-xs" href="' . route('admin.editBookHouse', [$allData->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this?\');"  href="' . route('admin.destroyBookHouse', [$allData->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroyBookHouse($id)
    {

        $delete = DB::table('allpost')->where('id', '=', $id)->first();
        $path = "ghunghur/public/images/Others/$delete->image";
        //dd($path);
        if(is_file($path)){
            unlink($path);
        }
        DB::table('allpost')->where('id', '=', $id)->delete();
        return redirect("admin/AllBookHouse")->with('message', 'Successfully Delete !!');

    }

    public function editBookHouse($id)
    {
        //
        $updateBookHouse = DB::table('allpost')->where('id', '=', $id)->first();
        return view('admin.updateBookHouse', compact('updateBookHouse'));
    }

    public function updateBookHouse(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/BookHouse'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('allpost')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Update!!');
    }





    public function publishedBookHouse($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllBookHouse")->with('message', 'Successfully Published!!');
    }


    public function unpublishedBookhouse($id){

        DB::table('allpost')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllBookHouse")->with('message', 'Successfully Unpublished!!');

    }
}

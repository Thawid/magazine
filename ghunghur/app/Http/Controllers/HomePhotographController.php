<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HomePhotograph;
use Illuminate\Support\Facades\DB;

class HomePhotographController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }


    public function fileCreate()

    {
        //dd('hello world');
        return view('admin.PhotographyUpload');
    }


    public function fileStore(Request $request)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images/Photography'),$imageName);

        $imageUpload = new HomePhotograph();
        $imageUpload->filename = $imageName;
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
    }

    public function viewHomePhotography()
    {
        $AllPhotography = DB::table('home_photographs')->get();
        return view('admin.viewHomePhotography', compact('AllPhotography'));
    }
    public function getHomePhotography(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $dataList = DB::table('home_photographs')->get();
        $data = array();
        foreach ($dataList as $allData) {

         $data[] = [

                $allData->created_at,
                '<img height="50" src="' . asset('ghunghur/public/images/Photography') . DIRECTORY_SEPARATOR . $allData->filename . '">',
                '<a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this?\');"  href="' . route('admin.fileDestroy', [$allData->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                ,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function fileDestroy($id)

    {

        $delete = DB::table('home_photographs')->where('id', '=', $id)->first();
        $path = "ghunghur/public/images/Photography/$delete->filename";
        //dd($path);
        if(is_file($path)){
            unlink($path);
        }
        DB::table('home_photographs')->where('id', '=', $id)->delete();
        return redirect("admin/viewHomePhotography")->with('message', 'Successfully Delete !!');


    }


}

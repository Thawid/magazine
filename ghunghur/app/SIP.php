<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SIP extends Model
{
    use Notifiable;

    protected $table = '';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sip_ip',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}

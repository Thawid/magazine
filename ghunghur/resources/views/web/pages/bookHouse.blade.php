<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 4/8/2019
 * Time: 6:49 PM
 */
?>

@extends('web.layouts.master')
@section('title', 'ঘুংঘুর ||  বই ঘর  ')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong>হোম <i class="fa fa-chevron-right"></i>  বই ঘর </strong>
                </div>
                 @php
                     $new = new \Illuminate\Support\Facades\DB();
                     $adv = \Illuminate\Support\Facades\DB::table('advertising')->where('type','=', 3)->orderBy('id','desc')->limit('1')->first();
                 @endphp

                @if(isset($adv->filename) == true)
    
                    <a href="javascript:void(0);"><img src="{{ secure_asset('ghunghur/public/images/Bigapon/'.$adv->filename) }}" alt="" /></a>
    
    
                @else
                    <p class="text-right"> </p>
    
                @endif
                <div class="composs-panel-inner">


                    <div class="composs-article-list lets-do-3">
                        {{--{{ dd($bookHouse) }}--}}
                        @foreach($bookHouse as $dataList)
                        <div class="item">
                            <div class="item-header">

                                <a href="javascript:void(0);"><img src="{{secure_asset('ghunghur/public/images/'.$dataList->type.'/'.$dataList->image) }}" alt="" /></a>
                            </div>
                            <div class="bookHouseItem">
                                <h3><a href="javascript:void(0);">{{ str_limit($dataList->title,27) }} </a></h3>
                                <p><a href="javascript:void(0);">  {{ $dataList->author_name }}  </a></p>

                                <!--<a href="#" class="ot-shortcode-button button button-outline" data-ot-css="border-color: #dd3333; color: #dd3333;" style="border-color: #dd3333; color: #dd3333;"> <i class="fa fa-paste left"></i> Order Now </a>--->
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
                <div class="composs-panel-pager">
                    {{ $bookHouse->links() }}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

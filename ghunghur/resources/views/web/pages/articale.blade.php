<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 2/24/2019
 * Time: 6:46 PM
 */
?>

@extends('web.layouts.master')
@section('title', 'ঘুংঘুর || প্রবন্ধ')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong>হোম <i class="fa fa-chevron-right"></i> প্রবন্ধ </strong>
                </div>

                <div class="composs-panel-inner">
               
                    <div class="composs-blog-list lets-do-3">
						
							@foreach($allArticale as $articale)
								<div class="item">
									<div class="item-header">

										<a href="{{ URL('/showArticle/'.$articale->id )}}"><img class="imgThumnil" src="{{ asset('ghunghur/public/images/'.$articale->type.'/'.$articale->image) }}" alt="" /></a>
									</div>
									<div class="item-content">
											<span class="item-meta">
												<span class="item-meta-item"><i class="fa fa-user"></i> {{ $articale->author_name }} </span>

											</span>
										<h2><a href="{{ URL('/showArticle/'.$articale->id )}}"> {{ str_limit($articale->title, 27) }} </a></h2>
										<span class="item-meta">
												<a href="{{ URL('/showArticle/'.$articale->id )}}" class="item-meta-item"><i class="material-icons">access_time</i> {{ $articale->published_date }} </a>
											 </span>
										<div class="shortcode-content">
											<p class="text-justify">
												{!!  str_limit(strip_tags($articale->post_body),135) !!}
											</p>
										</div>
										<div class="article_bottom">
											<a class="more" title="বিস্তারিত" href="{{ URL('/showArticle/'.$articale->id )}}"><span>বিস্তারিত</span>:::</a>
										</div>
									</div>
								</div>
							@endforeach
						
                    </div>
               
                </div>
                <div class="composs-panel-pager">
                    {{ $allArticale->links() }}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

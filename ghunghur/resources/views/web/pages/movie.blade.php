<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/5/2019
 * Time: 12:57 PM
 */
?>

@extends('web.layouts.master')
@section('title', 'ঘুংঘুর || চলচ্চিত্র')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong>হোম <i class="fa fa-chevron-right"></i> চলচ্চিত্র  </strong>
                </div>

                <div class="composs-panel-inner">

                    <div class="composs-blog-list lets-do-3">
                        @foreach($allMovie as $movieList)

                            <div class="item">
                                <div class="item-header">

                                    <a href="{{ URL('/showMovie/'.$movieList->id )}}"><img src="{{ asset('ghunghur/public/images/'.$movieList->type.'/'.$movieList->image) }}" alt="" /></a>
                                </div>
                                <div class="item-content">
                                        <span class="item-meta">
                                            <span class="item-meta-item"><i class="fa fa-user"></i> {{ $movieList->author_name }} </span>

                                        </span>
                                    <h2><a href="{{ URL('/showMovie/'.$movieList->id )}}"> {{ str_limit($movieList->title) }} </a></h2>
                                    <span class="item-meta">
                                            <a href="{{ URL('/showMovie/'.$movieList->id )}}" class="item-meta-item"><i class="material-icons">access_time</i> {{ $movieList->published_date }} </a>
                                         </span>
                                    <div class="shortcode-content">
                                        <p class="text-justify">
                                            {!!  str_limit(strip_tags($movieList->post_body,135)) !!}
                                        </p>
                                    </div>
                                    <div class="article_bottom">
                                        <a class="more" title="বিস্তারিত" href="{{ URL('/showMovie/'.$movieList->id )}}"><span>বিস্তারিত</span>:::</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="composs-panel-pager">
                    {{ $allMovie->links() }}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

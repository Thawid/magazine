<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/5/2019
 * Time: 11:09 AM
 */
?>
@extends('web.layouts.master')
@section('title', 'ঘুংঘুর || চিত্রকলা')
@section('meta-url')
    <meta property="og:url" content="{{ asset('/show'.$paintingDetails->type.'/'.$paintingDetails->id) }}" />
@endsection
@section('meta-data')
    <meta property="og:image"
          content="{{ asset('ghunghur/public/images/'.$paintingDetails->type.'/'.$paintingDetails->image) }}"/>
@endsection
@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <div class="theiaStickySidebar">

                <!-- BEGIN .composs-panel -->
                <div class="composs-panel">

                    <!-- <div class="composs-panel-title">
                        <strong>Blog page style #1</strong>
                    </div> -->

                    <div class="composs-panel-inner">

                        <div class="composs-main-article-content">
                           
                            <h1> {{ $paintingDetails->title }} 
                            <div class="button-left"><div class="fb-share-button" data-href="{{ asset('/show'.$paintingDetails->type.'/'.$paintingDetails->id) }}" data-layout="button_count" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fghunghur.com%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div></div>
                            </h1>

                            <div class="composs-main-article-head">
                                <div class="composs-main-article-media">
                                    <img src=" {{ asset('ghunghur/public/images/'.$paintingDetails->type.'/'.$paintingDetails->image) }} " alt="" />
                                </div>
                                <div class="composs-main-article-meta">
                                    <span class="item"><i class="fa fa-user"></i> {{ $paintingDetails->author_name }} </span>
                                    <a href="#comments" class="item"><i class="material-icons">access_time</i>প্রকাশিত: {{ $paintingDetails->published_date }} </a>
                                </div>
                                <div id="share"></div>
                            </div>

                            <div class="shortcode-content">

                                <p class="">
                                    {!! $paintingDetails->post_body !!}
                                </p>



                            </div>
                        </div>

                    </div>
                @include('web.layouts.include.disqus')
                    <!-- END .composs-panel -->
                </div>

                <!------  Read More Section --------------->

                <div class="composs-panel">

                    <div class="composs-panel-title">
                        <strong>  <span class="readMore"> আরও পড়ুন </span> </strong>
                    </div>
                    <div class="composs-panel-inner">

                        <div class="composs-blog-list lets-do-3">

                            @foreach($readMore as $moreArticale)
                                <div class="item">
                                    <div class="item-header">

                                        <a href="{{ URL('/showPainting/'.$moreArticale->id )}}"><img src="{{ asset('ghunghur/public/images/'.$moreArticale->type.'/'.$moreArticale->image) }}" alt="" /></a>
                                    </div>
                                    <div class="item-content">

                                        <h2><a href="{{ URL('/showPainting/'.$moreArticale->id )}}"> {{ $moreArticale->title }} - {{ $moreArticale->author_name }} </a></h2>

                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>

                <!----------  End Read More --------------------->

            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

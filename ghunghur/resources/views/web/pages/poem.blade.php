<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/4/2019
 * Time: 1:42 PM
 */
?>

@extends('web.layouts.master')
@section('title', 'ঘুংঘুর || কবিতা')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong>হোম <i class="fa fa-chevron-right"></i> কবিতা  </strong>
                </div>

                <div class="composs-panel-inner">

                    <div class="composs-blog-list lets-do-3">
                        @foreach($allPoem as $poemList)
                            <div class="item">
                                <div class="item-header imgThumnil">

                                    <a href="{{ URL('/showPoem/'.$poemList->id )}}"><img  src="{{ asset('ghunghur/public/images/'.$poemList->type.'/'.$poemList->image) }}" alt="" /></a>
                                </div>
                                <div class="item-content">
                                        <span class="item-meta">
                                            <span class="item-meta-item"><i class="fa fa-user"></i> {{ $poemList->author_name }} </span>

                                        </span>
                                    <h2><a href="{{ URL('/showPoem/'.$poemList->id )}}"> {{ str_limit($poemList->title,27) }} </a></h2>
                                    <span class="item-meta">
                                            <a href="{{ URL('/showPoem/'.$poemList->id )}}" class="item-meta-item"><i class="material-icons">access_time</i> {{ $poemList->published_date }} </a>
                                         </span>
                                    <div class="shortcode-content">
                                        <p class="text-justify">
                                            {!!  str_limit(strip_tags($poemList->post_body),135) !!}
                                        </p>
                                    </div>
                                    <div class="article_bottom">
                                        <a class="more" title="বিস্তারিত" href="{{ URL('/showPoem/'.$poemList->id )}}"><span>বিস্তারিত</span>:::</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="composs-panel-pager">
                    {{ $allPoem->links() }}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

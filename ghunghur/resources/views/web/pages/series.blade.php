<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/4/2019
 * Time: 3:26 PM
 */
?>
@extends('web.layouts.master')
@section('title', 'ঘুংঘুর || ধারাবাহিক')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong>হোম <i class="fa fa-chevron-right"></i> ধারাবাহিক  </strong>
                </div>

                <div class="composs-panel-inner">

                    <div class="composs-blog-list lets-do-3">
                        @foreach($allSeries as $seriesList)
                            <div class="item">
                                <div class="item-header">

                                    <a href="{{ URL('/showSeries/'.$seriesList->id )}}"><img src="{{ asset('ghunghur/public/images/'.$seriesList->type.'/'.$seriesList->image) }}" alt="" /></a>
                                </div>
                                <div class="item-content">
                                        <span class="item-meta">
                                            <span class="item-meta-item"><i class="fa fa-user"></i> {{ $seriesList->author_name }} </span>

                                        </span>
                                    <h2><a href="{{ URL('/showSeries/'.$seriesList->id )}}"> {{ str_limit($seriesList->title,27) }} </a></h2>
                                    <span class="item-meta">
                                            <a href="{{ URL('/showSeries/'.$seriesList->id )}}" class="item-meta-item"><i class="material-icons">access_time</i> {{ $seriesList->published_date }} </a>
                                         </span>
                                    <div class="shortcode-content">
                                        <p class="text-justify">
                                            {!!  strip_tags(str_limit($seriesList->post_body,135)) !!}
                                        </p>
                                    </div>
                                    <div class="article_bottom">
                                        <a class="more" title="বিস্তারিত" href="{{ URL('/showSeries/'.$seriesList->id )}}"><span>বিস্তারিত</span>:::</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="composs-panel-pager">
                    {{ $allSeries->links() }}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

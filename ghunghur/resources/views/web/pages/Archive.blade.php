<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 4/9/2019
 * Time: 9:55 AM
 */
?>


@extends('web.layouts.master')
@section('title', 'ঘুংঘুর ||  ই-ঘুংঘুর   ')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong>হোম <i class="fa fa-chevron-right"></i>  ই-ঘুংঘুর  </strong>
                </div>

                <div class="composs-panel-inner">

                    <div class="composs-archive-list lets-do-3">
                       {{-- {{ dd($archive) }}--}}
                        @foreach($archive as $dataList)

                            <div class="item">
                                <h3 class="item-title" data-ot-css="background-color: #dd4b39;"> {{ $dataList->number }}</h3>

                                <div class="ot-w-article-list">
                                    <div class="item">
                                        <div class="item-header">

                                            <a href="javascript:void(0);"><img src="{{ secure_asset('ghunghur/public/images/'.$dataList->type.'/'.$dataList->image) }}" alt=""></a>
                                        </div>
                                        <div class="item-content">
                                            <h4><a href="javascript:void(0);"> {{ $dataList->title }}</a></h4>
                                            <span class="item-meta">
												<span class="item-meta-item"><i class="material-icons">access_time</i> {{ $dataList->published_date }}</span>
											</span>
                                        </div>
                                    </div>

                                    <a href="{{ secure_asset('ghunghur/public/Archive/'.$dataList->filename) }}" target="_blank" class="item footer-button">Download File</a>

                                </div>


                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="composs-panel-pager">
                    {{ $archive->links() }}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection
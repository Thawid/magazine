@extends('web.layouts.master')
@section('title',"ঘুংঘুর")
@section('meta-url')
    <meta property="og:url" content="{{ asset('/home') }}" />
@endsection
@section('meta-data')
    <meta property="og:image"
          content=""/>
@endsection
@section('content')
    <div class="content-wrapper">

						<!-- BEGIN .composs-main-content -->
						<div class="composs-main-content composs-main-content-s-1">

							<div class="theiaStickySidebar">

								<!-- BEGIN .composs-panel -->
								<div class="composs-panel">

									<!-- <div class="composs-panel-title">
										<strong>Blog page style #1</strong>
									</div> -->

									<div class="composs-panel-inner">

										<div class="composs-main-article-content">

											<h1> {{$lastPost->title }}</h1>

											<div class="composs-main-article-head">
												<div class="composs-main-article-media">
													<img src="{{secure_asset('ghunghur/public/images/'.$lastPost->type.'/'.$lastPost->image)}}" alt="" />
												</div>
												<div class="composs-main-article-meta">
													<span class="item"><i class="fa fa-user"></i> {{ $lastPost->author_name }} </span>
													<a href="#comments" class="item"><i class="material-icons">access_time</i>প্রকাশিত: {{$lastPost->published_date}} </a>
												</div>

											</div>

											<div class="shortcode-content">
											    <p>
											{!! str_limit(strip_tags($lastPost->post_body),500)  !!}
											</p>
											</div>
											<div class="article_bottom">
												<a class="more" title="বিস্তারিত" href="{{ URL('/show'.$lastPost->type.'/'.$lastPost->id )}}"><span>বিস্তারিত</span>:::</a>
											</div>
											
											
										</div>

									</div>
									<div class="composs-panel">

									

										<div class="composs-panel-inner">

											<div class="composs-blog-list lets-do-1">
											@foreach($recentText as $article)
												<div class="item">
													<div class="item-header">
														
														<a href="{{ URL('/show'.$article->type.'/'.$article->id )}}">
															<img src="{{secure_asset('ghunghur/public/images/'.$article->type.'/'.$article->image)}}" alt="" />
														</a>
													</div>
													<div class="item-content">
														<h2>
															<a href="{{ URL('/show'.$article->type.'/'.$article->id )}}">{{ $article->title }} </a>
														</h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>{{ $article->published_date }}</span>
														</span>
														<p class="allPharagraph">
														{!! str_limit(strip_tags($article->post_body), 300) !!}
														</p>
													</div>
												</div>
											@endforeach
											</div>

										</div>
									</div>
									<div class="composs-panel">

										<div class="composs-panel-title composs-panel-title-tabbed">
											<strong class="active">  ছোট গল্প  </strong>

										</div>

										<div class="composs-panel-inner">
											<div class="composs-panel-tab active">

												<div class="composs-article-split-block">

													<div class="item-large">

														<div class="item">
															<div class="item-header">

																<a class="" href="{{ URL('/show'.$singleShortStory->type.'/'.$singleShortStory->id )}}"><img src="{{secure_asset('ghunghur/public/images/'.$singleShortStory->type.'/'.$singleShortStory->image)}}" alt=""/></a>
															</div>
															<div class="item-content">
																<h2><a href="{{ URL('/show'.$singleShortStory->type.'/'.$singleShortStory->id )}}"> {{ $singleShortStory->title }}  </a></h2>
																<span class="item-meta">
																					<span class="item-meta-item"><i class="material-icons">access_time</i> প্রকাশিত : {{ $singleShortStory->published_date }} </span>
																				</span>
																<p class="allPharagraph">
																	{!! str_limit(strip_tags($singleShortStory->post_body),150) !!}
																</p>
															</div>
														</div>

													</div>

													<div class="item-small">
													
													@foreach($shortPost as $shortStory)
														<div class="item">
															<div class="item-header">

																<a  href="{{ URL('/show'.$shortStory->type.'/'.$shortStory->id )}}"><img class="shortStoryImage" src="{{ secure_asset('ghunghur/public/images/'.$shortStory->type.'/'.$shortStory->image) }}" alt=""/></a>
															</div>
															<div class="item-content">
																<a class="customeContent" href="{{ URL('/show'.$shortStory->type.'/'.$shortStory->id )}}"> {{ $shortStory->title }} </a>
																<span class="item-meta">
																	<span class="item-meta-item"><i class="material-icons">access_time</i> প্রকাশিত : {{ $shortStory->published_date }} </span>
																</span>
															</div>
														</div>
													@endforeach
													
													</div>

												</div>

											</div>
										</div>
									</div>
									<div class="composs-panel">

										<div class="composs-panel-title composs-panel-title-tabbed">
											<strong class="active"> প্রবন্ধ  </strong>
											<strong> কবিতা </strong>
											<strong> ধারাবাহিক </strong>
										</div>

										<div class="composs-panel-inner">
											<div class="composs-panel-tab active">

												<div class="composs-article-split-block">

													<div class="item-large">

														<div class="item">
															<div class="item-header">

																<a href="{{ URL('/show'.$singleArticle->type.'/'.$singleArticle->id )}}"><img src="{{ secure_asset('ghunghur/public/images/'.$singleArticle->type.'/'.$singleArticle->image) }}" alt=""/></a>
															</div>
															<div class="item-content">

																<h2><a href="{{ URL('/show'.$singleArticle->type.'/'.$singleArticle->id )}}"> {{ $singleArticle->title }}</a></h2>
																<span class="item-meta">
																	<span class="item-meta-item"><i class="material-icons">access_time</i> প্রকাশিত : {{ $singleArticle->published_date }} </span>
																</span>
																<p class="allPharagraph">
																	{!! str_limit(strip_tags($singleArticle->post_body), 120) !!}
																</p>

															</div>
														</div>
													</div>

													<div class="item-small">
														@foreach($recentArticle as $article)
															<div class="item">
																<div class="item-header">

																	<a href="{{ URL('/show'.$article->type.'/'.$article->id )}}"><img class="shortStoryImage" src="{{ secure_asset('ghunghur/public/images/'.$article->type.'/'.$article->image) }}" alt=""/></a>
																</div>
																<div class="item-content">
																	<a  class="customeContent" href="{{ URL('/show'.$article->type.'/'.$article->id )}}"> {{ $article->title }} </a>
																	<span class="item-meta">
																		 <span class="item-meta-item"><i class="material-icons">access_time</i>প্রকাশিত : {{ $article->published_date }}</span>
																	</span>
																</div>
															</div>
														@endforeach
													</div>

												</div>

											</div>
											<div class="composs-panel-tab">

												<div class="composs-article-split-block">

													<div class="item-large">

														<div class="item">
															<div class="item-header">

																<a href="{{ URL('/show'.$singlePoeams->type.'/'.$singlePoeams->id )}}"><img src="{{ secure_asset('ghunghur/public/images/'.$singlePoeams->type.'/'.$singlePoeams->image) }}" alt="" /></a>
															</div>
															<div class="item-content">
																<h2><a href="{{ URL('/show'.$singlePoeams->type.'/'.$singlePoeams->id )}}"> {{ $singlePoeams->title }} </a></h2>
																<span class="item-meta">

																		<span class="item-meta-item"><i class="material-icons">access_time</i> {{ $singlePoeams->published_date }}</span>


																</span>
																<p class="allPharagraph">
																	{!! str_limit(strip_tags($singlePoeams->post_body), 120) !!}
																</p>


															</div>
														</div>

													</div>

													<div class="item-small">

														@foreach($Poeams as $poem)
															<div class="item">
																<div class="item-header">

																	<a href="{{ URL('/show'.$poem->type.'/'.$poem->id )}}"><img class="shortStoryImage" src="{{ secure_asset('ghunghur/public/images/'.$poem->type.'/'.$poem->image) }}" alt=""/></a>
																</div>
																<div class="item-content">
																	<a  class="customeContent" href="{{ URL('/show'.$poem->type.'/'.$poem->id )}}"> {{ $poem->title }} </a>
																	<span class="item-meta">
																		   <span class="item-meta-item"><i class="material-icons">access_time</i> প্রকাশিত : {{ $poem->published_date }}</span>
																	</span>

																</div>
															</div>
														@endforeach

													</div>
												</div>

											</div>
											<div class="composs-panel-tab">

												<div class="composs-article-split-block">
												@if(isset($singleSeries->type))
													<div class="item-large">

														<div class="item">
															<div class="item-header">

																<a href="{{ URL('/show'.$singleSeries->type.'/'.$singleSeries->id )}}"><img src="{{ secure_asset('ghunghur/public/images/'.$singleSeries->type.'/'.$singleSeries->image) }}" alt="" /></a>
															</div>
															<div class="item-content">
																<h2><a href="{{ URL('/show'.$singleSeries->type.'/'.$singleSeries->id )}}"> {{ $singleSeries->title }} </a></h2>
																<span class="item-meta">

																		<span class="item-meta-item"><i class="material-icons">access_time</i> {{ $singleSeries->published_date }}</span>


																</span>
																<p class="allPharagraph">
																	{!! str_limit(strip_tags($singleSeries->post_body), 120) !!}
																</p>


															</div>
														</div>

													</div>
												 @endif
													<div class="item-small">
														@foreach($Series as $postSeries)
															<div class="item">
																<div class="item-header">

																	<a href="{{ URL('/show'.$postSeries->type.'/'.$postSeries->id )}}"><img class="shortStoryImage" src="{{ secure_asset('ghunghur/public/images/'.$postSeries->type.'/'.$postSeries->image) }}" alt=""/></a>
																</div>
																<div class="item-content">
																	<a  class="customeContent" href="{{ URL('/show'.$postSeries->type.'/'.$postSeries->id )}}"> {{ $postSeries->title }} </a>
																	<span class="item-meta">
																			<span class="item-meta-item"><i class="material-icons">access_time</i>প্রকাশিত : {{ $postSeries->published_date }}</span>
																	</span>

																</div>
															</div>
														@endforeach
													</div>
												</div>

											</div>
										</div>


									</div>
									<div class="composs-panel">
										<div class="composs-panel-title">
											<strong>আলোকচিত্র </strong>
										</div>


										<div class="composs-panel-inner">

											<div class="widget-content ot-w-gallery-list">

												<!-- BEGIN .item -->
												<div class="item size-700">

													<div class="item-header slider-art">
														{{--{{ dd($photograph) }}--}}
														@foreach($photograph as $photoList)
															<img class="homeGallery  img img-responsive" src="{{secure_asset('ghunghur/public/images/Photography/'.$photoList->filename)}}" alt="" />
														@endforeach
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="composs-panel">


										<div class="composs-panel-title">
											<strong>চিত্রকলা </strong>
										</div>
										<div class="composs-panel-inner">

											<div class="widget-content ot-w-gallery-list">

												<!-- BEGIN .item -->
												<div class="item size-700">

													<div class="item-header slider-art">
														@foreach($painting as $paintList)
															<img class="homeGallery  img img-responsive" src="{{secure_asset('ghunghur/public/images/Painting/'.$paintList->filename)}}" alt="" />
														@endforeach
													</div>
												</div>

											</div>
										</div>
									</div>

								<!-- END .composs-panel -->
								</div>

							</div>

						<!-- END .composs-main-content -->
							
						</div>
						
						<!-- BEGIN #sidebar -->
						

						@include('web.layouts.include.sidebar')

					@endsection
			
<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/5/2019
 * Time: 12:10 PM
 */
?>
@extends('web.layouts.master')
@section('title', 'ঘুংঘুর || অনুবাদ')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong> হোম <i class="fa fa-chevron-right"></i> অনুবাদ  </strong>
                </div>

                <div class="composs-panel-inner">

                    <div class="composs-blog-list lets-do-3">
						
							@foreach($allTranslate as $translateList)

								<div class="item">
									<div class="item-header">

										<a href="{{ URL('/showTranslate/'.$translateList->id )}}"><img src="{{ asset('ghunghur/public/images/'.$translateList->type.'/'.$translateList->image) }}" alt="" /></a>
									</div>
									<div class="item-content">
											<span class="item-meta">
												<span class="item-meta-item"><i class="fa fa-user"></i> {{ $translateList->author_name }} </span>

											</span>
										<h2><a href="{{ URL('/showTranslate/'.$translateList->id )}}"> {{ $translateList->title }} </a></h2>
										<span class="item-meta">
												<a href="{{ URL('/showTranslate/'.$translateList->id )}}" class="item-meta-item"><i class="material-icons">access_time</i> {{ $translateList->published_date }} </a>
											 </span>
										<div class="shortcode-content">
											<p class="text-justify">
												{!!  str_limit(strip_tags($translateList->post_body),120) !!}
											</p>
										</div>
										<div class="article_bottom">
											<a class="more" title="বিস্তারিত" href="{{ URL('/showTranslate/'.$translateList->id )}}"><span>বিস্তারিত</span>:::</a>
										</div>
									</div>
								</div>
							@endforeach
						
                    </div>

                </div>
                <div class="composs-panel-pager">
                    {{ $allTranslate->links() }}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

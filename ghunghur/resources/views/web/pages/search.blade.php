<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/10/2019
 * Time: 12:33 PM
 */
?>

@extends('web.layouts.master')
@section('title', 'ঘুংঘুর || অনুসন্ধানের ফলাফল ')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong>হোম <i class="fa fa-chevron-right"></i> অনুসন্ধানের ফলাফল   </strong>
                </div>

                <div class="composs-panel-inner">

                    @if(count($search)>'0')
                        <div class="composs-blog-list lets-do-3">
                            @foreach($search as $result)

                                <div class="item">
                                    <div class="item-header">

                                        <a href="{{ URL('/show'.$result->type.'/'.$result->id )}}"><img class="img-responsive post-image" src="{{ asset('ghunghur/public/images/'.$result->type.'/'.$result->image) }}" alt="" /></a>
                                    </div>
                                    <div class="item-content">
                                        <span class="item-meta">
                                            <span class="item-meta-item"><i class="fa fa-user"></i> {{ $result->author_name }} </span>

                                        </span>
                                        <h2><a href="{{ URL('/show'.$result->type.'/'.$result->id )}}"> {{ str_limit($result->title,28) }} </a></h2>
                                        <span class="item-meta">
                                            <a href="{{ URL('/show'.$result->type.'/'.$result->id )}}" class="item-meta-item"><i class="material-icons">access_time</i> {{ $result->published_date }} </a>
                                         </span>
                                        <div class="shortcode-content">
                                            <p class="text-justify">
                                                {!!  str_limit(strip_tags($result->post_body),120) !!}
                                            </p>
                                        </div>
                                        <div class="article_bottom">
                                            <a class="more" title="বিস্তারিত" href="{{ URL('/show'.$result->type.'/'.$result->id )}}"><span>বিস্তারিত</span>:::</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <h2> পাওয়া যায়নি </h2>
                        <p>
                            আপনি যে বিষয়টি অনুসন্ধান করছেন তা খুজে পাওয়া যায়নি। বিষয়টি সম্ভবত আমাদের  সাথে সংশ্লিষ্ট নয় অথবা আপনি ভুলভাবে অনুসন্ধান করছেন। অনুগ্রহ করে আপনার অনুসন্ধান বিষয়টি সম্বন্ধে নিশ্চিত হোন।

                        </p>
                        <p>

                            ধন্যবাদ।
                        </p>
                    @endif

                </div>
                <div class="composs-panel-pager">
                   {{-- {{ $allSeries->links() }}--}}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/5/2019
 * Time: 1:05 PM
 */
?>
@extends('web.layouts.master')
@section('title', 'ঘুংঘুর || নাটক')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong>হোম <i class="fa fa-chevron-right"></i> নাটক  </strong>
                </div>

                <div class="composs-panel-inner">

                    <div class="composs-blog-list lets-do-3">
                        @if(isset($allDrama))
                        @foreach($allDrama as $dramaList)

                            <div class="item">
                                <div class="item-header">

                                    <a href="{{ URL('/showDrama/'.$dramaList->id )}}"><img src="{{ asset('ghunghur/public/images/'.$dramaList->type.'/'.$dramaList->image) }}" alt="" /></a>
                                </div>
                                <div class="item-content">
                                        <span class="item-meta">
                                            <span class="item-meta-item"><i class="fa fa-user"></i> {{ $dramaList->author_name }} </span>

                                        </span>
                                    <h2><a href="{{ URL('/showDrama/'.$dramaList->id )}}"> {{ str_limit($dramaList->title, 27) }} </a></h2>
                                    <span class="item-meta">
                                            <a href="{{ URL('/showDrama/'.$dramaList->id )}}" class="item-meta-item"><i class="material-icons">access_time</i> {{ $dramaList->published_date }} </a>
                                         </span>
                                    <div class="shortcode-content">
                                        <p class="text-justify">
                                            {!!  str_limit(strip_tags($dramaList->post_body),135) !!}
                                        </p>
                                    </div>
                                    <div class="article_bottom">
                                        <a class="more" title="বিস্তারিত" href="{{ URL('/showDrama/'.$dramaList->id )}}"><span>বিস্তারিত</span>:::</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @endif
                    </div>

                </div>
                <div class="composs-panel-pager">
                    {{ $allDrama->links() }}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

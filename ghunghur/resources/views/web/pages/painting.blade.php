<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/5/2019
 * Time: 10:44 AM
 */
?>
@extends('web.layouts.master')
@section('title', 'ঘুংঘুর || চিত্রকলা')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong>হোম <i class="fa fa-chevron-right"></i> চিত্রকলা  </strong>
                </div>

                <div class="composs-panel-inner">

                    <div class="composs-blog-list lets-do-3">
                        @foreach($allPainting as $paintingList)

                            <div class="item">
                                <div class="item-header">

                                    <a href="{{ URL('/showPainting/'.$paintingList->id )}}"><img src="{{ asset('ghunghur/public/images/'.$paintingList->type.'/'.$paintingList->image) }}" alt="" /></a>
                                </div>
                                <div class="item-content">
                                        <span class="item-meta">
                                            <span class="item-meta-item"><i class="fa fa-user"></i> {{ $paintingList->author_name }} </span>

                                        </span>
                                    <h2><a href="{{ URL('/showPainting/'.$paintingList->id )}}"> {{ str_limit($paintingList->title,27) }} </a></h2>
                                    <span class="item-meta">
                                            <a href="{{ URL('/showPainting/'.$paintingList->id )}}" class="item-meta-item"><i class="material-icons">access_time</i> {{ $paintingList->published_date }} </a>
                                         </span>
                                    <div class="shortcode-content">
                                        <p class="text-justify">
                                            {!!  str_limit(strip_tags($paintingList->post_body),135) !!}
                                        </p>
                                    </div>
                                    <div class="article_bottom">
                                        <a class="more" title="বিস্তারিত" href="{{ URL('/showPainting/'.$paintingList->id )}}"><span>বিস্তারিত</span>:::</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="composs-panel-pager">
                    {{ $allPainting->links() }}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

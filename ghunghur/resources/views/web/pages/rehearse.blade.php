<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 4/7/2019
 * Time: 1:01 PM
 */
?>

@extends('web.layouts.master')
@section('title', 'ঘুংঘুর || পুনর্পাঠ  ')

@section('content')

    <div class="content-wrapper">

        <!-- BEGIN .composs-main-content -->
        <div class="composs-main-content composs-main-content-s-1">

            <!-- BEGIN .composs-panel -->
            <div class="composs-panel">

                <div class="composs-panel-title">
                    <strong>হোম <i class="fa fa-chevron-right"></i> পুনর্পাঠ    </strong>
                </div>

                <div class="composs-panel-inner">

                    <div class="composs-blog-list lets-do-3">
                        {{--{{ //dd($Muktogdy) }}--}}
                        @foreach($rehearse as $dataList)

                            <div class="item">
                                <div class="item-header">

                                    <a href="{{ URL('/showRehearse/'.$dataList->id )}}"><img src="{{ asset('ghunghur/public/images/'.$dataList->type.'/'.$dataList->image) }}" alt="" /></a>
                                </div>
                                <div class="item-content">
                                        <span class="item-meta">
                                            <span class="item-meta-item"><i class="fa fa-user"></i> {{ $dataList->author_name }} </span>

                                        </span>
                                    <h2><a href="{{ URL('/showRehearse/'.$dataList->id )}}"> {{ str_limit($dataList->title,27) }} </a></h2>
                                    <span class="item-meta">
                                            <a href="{{ URL('/showRehearse/'.$dataList->id )}}" class="item-meta-item"><i class="material-icons">access_time</i> {{ $dataList->published_date }} </a>
                                         </span>
                                    <div class="shortcode-content">
                                        <p class="text-justify">
                                            {!!  str_limit(strip_tags($dataList->post_body,135)) !!}
                                        </p>
                                    </div>
                                    <div class="article_bottom">
                                        <a class="more" title="বিস্তারিত" href="{{ URL('/showRehearse/'.$dataList->id )}}"><span>বিস্তারিত</span>:::</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="composs-panel-pager">
                    {{ $rehearse->links() }}
                </div>
            </div>

            <!-- END .composs-main-content -->
        </div>
    @include('web.layouts.include.sidebar')


@endsection

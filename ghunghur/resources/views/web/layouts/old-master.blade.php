<?php
if (isset($_SERVER['HTTPS']) &&
    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
  $protocol = 'https://';
}
else {
  $protocol = 'http://';
}
$notssl = 'http://';
if($protocol==$notssl){
    $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
    <script> 
    window.location.href ='<?php echo $url?>';
    </script> 
 <?php } ?>
<!DOCTYPE HTML>
<!-- BEGIN html -->
<html lang = "en">
<!-- BEGIN head -->

<head>
    <title>@yield('title')</title>

    <!-- Meta Tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ secure_asset('images/www.ghunghur.com-ghunghuricon.png') }}" type="image/x-icon" />

    <!-- Stylesheets -->
  
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link type="text/css" rel="stylesheet" href="{{secure_asset('css/reset.min.css')}}" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    {{--<link type="text/css" rel="stylesheet" href="{{secure_asset('css/bootstrap.min.css')}}" />--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
     <link type="text/css" rel="stylesheet" href="{{secure_asset('css/owl.carousel.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{secure_asset('css/animate.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{secure_asset('css/shortcodes.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{secure_asset('css/main-stylesheet.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{secure_asset('css/responsive.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{secure_asset('css/custom.css')}}" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--[if lte IE 8]>
    <link type="text/css" rel="stylesheet" href="{{secure_asset('css/ie-ancient.min.css')}}" />

    <![endif]-->


    <!-- END head -->

</head>

<body class="ot-menu-will-follow">
<!--<div class="page-loader">

	<div class="spinner"></div>
</div>-->

<!-- BEGIN .boxed -->
<div class="boxed">

    <!-- BEGIN .header -->
    <div class="header">

        <!-- BEGIN .wrapper -->

        @include('web.layouts.include.menu')

        <!-- END .header -->
    </div>
    <!-- BEGIN .content -->
    <div class="content">

        <!-- BEGIN .wrapper -->
        <div class="wrapper">

            @yield('content')
            <!-- BEGIN #footer -->
                <footer id="footer">

                    <!-- BEGIN .wrapper -->
                    <div class="wrapper">
                        <div class="call-md-6">
                            <img src="{{secure_asset('images/footerBootomImage.JPG')}}" alt="">
                        </div>

                        <div class="footer-widgets lets-do-4 footerArea">


                            <div class="widget-split item">
                                <div class="widget">
                                    <p>
                                        <a href="javascript:void(0);">
                                            <img class="footerLogo" src="{{secure_asset('images/logo.png')}}" data-ot-retina="{{secure_asset('images/logo.png')}}" alt="" />
                                        </a>
                                    </p>
                                </div>
                            </div>


                            <div class="widget-split item">
                                <div class="widget footerWidget">
                                    <h3>  অক্ষরে অক্ষরে শিল্প নির্মাণ </h3><br/>
                                    <h6> www.ghunghur.com </h6>

                                </div>
                            </div>

                            <div class="widget-split item">
                                <div class="widget footerWidget">
                                    <h3>সম্পাদক : হুমায়ূন   কবির </h3><br/>
                                    <h6>humayun.ghunghur@gmail.com</h6>

                                </div>
                            </div>

                            <div class="widget-split item">
                                <div class="widget footerWidget">
                                    <h3>নির্বাহী সম্পাদক : খালেদ চৌধুরী </h3><br/>
                                    <h6>khaled.ghunghur@gmail.com</h6>
                                </div>
                            </div>

                        </div>
                        <!--<div class="footerBootom footer-widgets lets-do-4">

                            <div class="widget-split item">
                                <div class="widget">
                                    <div>
                                        <p class="footerText">১৮৮৩ সালে কাহলিল জিবরান লেবাননের উত্তরে এক গ্রামে জন্মগ্রহণ করেন। তার পিতা কাহলিল; ট্যাক্স কালেক্টর ছিলেন। মায়ের নাম ছিল ক্যামিলা </p>

                                    </div>
                                </div>
                            </div>

                            <div class="widget-split item">
                                <div class="widget">

                                    <p class="footerText">১৮৮৩ সালে কাহলিল জিবরান লেবাননের উত্তরে এক গ্রামে জন্মগ্রহণ করেন। তার পিতা কাহলিল; ট্যাক্স কালেক্টর ছিলেন। মায়ের নাম ছিল ক্যামিলা </p>
                                </div>
                            </div>

                            <div class="widget-split item">
                                <div class="widget">

                                    <div class="widget-content ot-w-article-list">

                                        <p class="footerText"><i class="fa  fa-envelope"> </i> name.yourdomain@gmail.com</p>
                                        <p class="footerText"><i class="fa  fa-envelope"></i> name.yourdomain@gmail.com</p>

                                    </div>
                                </div>
                            </div>

                            <div class="widget-split item">
                                <div class="widget">

                                    <p class="footerText">১৮৮৩ সালে কাহলিল জিবরান লেবাননের উত্তরে এক গ্রামে জন্মগ্রহণ করেন। তার পিতা কাহলিল; ট্যাক্স কালেক্টর ছিলেন। মায়ের নাম ছিল ক্যামিলা </p>
                                </div>
                            </div>

                        </div>-->

                        <div class="footer-copyright">
                            <p class="copyrightText">যোগাযোগ : বসতি রিসোর্ট (সি-৪), বাড়ি নং: ৫২, রোড নং : ২৮, গুলশান-১  ঢাকা-১২১২ -- মুঠোফোন : ০১৭৮১-৯০২৭৪৪</p>
                        </div>

                        <!-- END .wrapper -->
                    </div>

                    <!-- END #footer -->
                </footer>

                <div class="ot-follow-share">
                    <a href="https://www.facebook.com/ghungghur/" target="_blank" class="ot-color-facebook" data-h-title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/" target="_blank" class="ot-color-twitter" data-h-title="Twitter"><i class="fa fa-twitter"></i></a>

                    <a href="https://www.youtube.com/" target="_blank" class="ot-color-youtube" data-h-title="Youtube"><i class="fa fa-youtube"></i></a>
                </div>

                <div class="ot-responsive-menu-header">
                    <a href="#" class="ot-responsive-menu-header-burger"><i class="material-icons">menu</i></a>
                    <a href="index.html" class="ot-responsive-menu-header-logo"><img src="{{secure_asset('images/logo.png')}}" alt="" /></a>
                </div>

                <!-- END .boxed -->
            </div>

            <div class="ot-responsive-menu-content-c-header">
                <a href="#" class="ot-responsive-menu-header-burger"><i class="material-icons">menu</i></a>
            </div>
            <div class="ot-responsive-menu-content">
                <div class="ot-responsive-menu-content-inner has-search">

                    <ul id="responsive-menu-holder">
                        <li>
                            <form class="navbar-form" role="search" action="{{ url('search') }}">
                                {{ csrf_field() }}
                                <input type="text" value="" placeholder="অনুসন্ধান" name="search" id="search"/>
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="ot-responsive-menu-background"></div>

            <!-- Scripts -->

            <script type="text/javascript" src="{{secure_asset('js/jquery.js')}}"></script>
            <script type="text/javascript" src="{{secure_asset('js/jquery-latest.min.js')}}"></script>
            <script type="text/javascript" src="{{secure_asset('js/bootstrap.min.js')}}"></script>
            <script type="text/javascript" src="{{secure_asset('js/theia-sticky-sidebar.min.js')}}"></script>
            <script type="text/javascript" src="{{secure_asset('js/modernizr.js')}}"></script>
            <script type="text/javascript" src="{{secure_asset('js/owl.carousel.min.js')}}"></script>
            <script type="text/javascript" src="{{secure_asset('js/shortcode-scripts.min.js')}}"></script>
            <script type="text/javascript" src="{{secure_asset('js/theme-scripts.min.js')}}"></script>
            <script type="text/javascript" src="{{secure_asset('js/ot-lightbox.min.js')}}"></script>
             <script type="text/javascript" src="{{secure_asset('cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script>
            <script>
                jQuery('.slider-owl').owlCarousel({
                    margin: 20,
                    responsiveClass: true,
                    items: 1,
                    nav: false,
                    dots: false,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: false
                });
            </script>
            <script>
                jQuery('.slider-art').owlCarousel({
                    margin: 20,
                    responsiveClass: true,
                    items: 1,
                    nav: false,
                    dots: false,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: false
                });
            </script>

            <script>
                jQuery(window).on('load', function () {
                    jQuery(".page-loader").fadeOut();
                    jQuery(".no-overflow-body").removeClass('no-overflow-body');
                    var d = document, s = d.createElement('script');
                    s.src = 'https://ghungghur.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                });
            </script>
       


            <!-- Demo Only -->


            <!-- END body -->
</body>
<!-- END html -->
</html>

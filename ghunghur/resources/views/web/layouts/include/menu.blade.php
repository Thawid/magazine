<div class="wrapper">

    <nav class="header-top">
        <div class="header-top-socials">

            <a href="https://www.facebook.com/ghungghur/" target="_blank" class="ot-color-hover-facebook"><i class="fa fa-facebook"></i></a>
            <a href="https://twitter.com/" target="_blank" class="ot-color-hover-twitter"><i class="fa fa-twitter"></i></a>

            <a href="https://www.youtube.com/" target="_blank" class="ot-color-hover-youtube"><i class="fa fa-youtube"></i></a>

        </div>

        <ul>
            <li>
                <form class="navbar-form" role="search" action="{{ url('search') }}">
                    {{ csrf_field() }}
                    <div class="input-group add-on">

                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                        <input class="form-control" placeholder="অনুসন্ধান" name="search" id="search" type="text">
                    </div>
                </form>
            </li>

            <li>
                @php

                    $bongabda = new \EasyBanglaDate\Types\BnDateTime(\Illuminate\Support\Carbon::now('Asia/Dhaka'), new DateTimeZone('Asia/Dhaka'));

                    $eng = $bongabda->getDateTime()->format('l, jS F Y'). PHP_EOL;
                    $ban = $bongabda->getDateTime()->enFormat('l jS F Y') . PHP_EOL;
                    $bangla = $bongabda->format('jS F Y b h:i:s') . PHP_EOL ;
                @endphp
                <a href="javascript:void(0);"> <p class="text-center" style="font-size: 16px;"> {{ $eng }} {{' - '}}{{ $bangla }} </p> </a>
            </li>

        </ul>


    </nav>

    <div class="header-content">

        <div class="header-content-logo">
            <a href="{{ secure_url('home') }}"><img class="headerlogo" src="{{ secure_asset('images/www.ghunghur-logo.png') }}" data-ot-retina="images/logo@2x.png" alt="" /></a>
        </div>

        <div class="header-content-o">
            @php
                $new = new \Illuminate\Support\Facades\DB();
                $adv = \Illuminate\Support\Facades\DB::table('advertising')->where('type','=', 1)->orderBy('id','desc')->limit('1')->first();
             @endphp

            @if(isset($adv->filename) == true)

                <a href="javascript:void(0);"><img src="{{ secure_asset('ghunghur/public/images/Bigapon/'.$adv->filename) }}" alt="" /></a>


            @else
                <p class="text-right"> </p>

            @endif


        </div>
    </div>

    <div class="main-menu-placeholder wrapper">
        <nav id="main-menu">
            <ul>
                <li @if(URL::current()==secure_url('home'))  @endif><a href="{{secure_url('home')}}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a></li>

                <li @if(URL::current()==secure_url('article')) @endif>
                    <a href="{{secure_url('article')}}"> প্রবন্ধ </a>
                </li>

                <li @if(URL::current()==secure_url('poem')) @endif><a href="{{secure_url('poem')}}"> কবিতা </a> </li>

                <li @if(URL::current()==secure_url('story')) @endif><a href="{{ secure_url('story') }}"> ছোট গল্প </a></li>
                <li><a href="{{ secure_url('series') }}"> ধারাবাহিক</a></li>
                <li><a href="{{ secure_url('interview') }}"> সাক্ষাৎকার </a></li>
                <li><a href="{{ secure_url('Muktogdy') }}"> মুক্তগদ্য </a></li>
                <li><a href="{{ secure_url('SahitySongbad') }}"> সাহিত্য সংবাদ  </a></li>

                <li><a href="{{ secure_url('painting') }}"> চিত্রকলা  </a></li>
                <li><a href="{{ secure_url('translate') }}"> অনুবাদ  </a></li>
                <li><a href=" {{ secure_url('special') }}"> বিশেষ সংখ্যা </a></li>
                <li><a href=" {{ secure_url('rehearse') }}"> পুনর্পাঠ    </a></li>
                <li><a href="{{ secure_url('bookHouse') }}"> বই ঘর    </a></li>

                <li><a href=" {{ secure_url('books') }}"> বইপত্র    </a></li>
                <li><a href=""><span>বিবিধ</span></a>
                    <ul class="sub-menu">
                        <li><a href="{{ secure_url('rhyme') }}">  ছড়া     </a></li>
                        <li><a href="{{ secure_url('drama') }}">  নাটক     </a></li>
                        <li><a href="{{ secure_url('movie') }}">    চলচ্চিত্র    </a></li>
                        <li><a href="{{ secure_url('others') }}"> অন্যান্য    </a></li>
                    </ul>
                </li>
                <li class="menuLastTab"><a href="{{ secure_url('Archive')}}"> ই-ঘুংঘুর  </a></li>


            </ul>

        </nav>
    </div>

    <!-- END .wrapper -->
</div>
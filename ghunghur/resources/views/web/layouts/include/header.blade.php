<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!--- Google Fonts ------>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
    <!---- Font Awesome ---->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- reset min css -->
    <link rel="stylesheet" href="{{asset('css/reset.min.css')}}">

    <!---- Bootstrap Css  ------>

    {{--<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">--}}
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!---- Owl carousel  ------>

    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">

    <!---- animate css  ------>

    <link rel="stylesheet" href="{{asset('css/animate.css')}}">

    <!---- short codes  css  ------>

    <link rel="stylesheet" href="{{asset('css/shortcodes.min.css')}}">

    <!---- Main style sheet  css  ------>

    <link rel="stylesheet" href="{{asset('main-stylesheet.min.css')}}">

    <!---- Responsive  css  ------>

    <link rel="stylesheet" href="{{asset('css/responsive.min.css')}}">

    <!---- Custom  css  ------>

    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

</head>
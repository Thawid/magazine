<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 2/24/2019
 * Time: 6:51 PM
 */
?>

<!-- BEGIN #sidebar -->
<aside id="sidebar">

    <!-- BEGIN .widget -->
    <div class="widget">
        <h3> সাম্প্রতিক লেখা  </h3>
        <div class="widget-content ot-w-gallery-list">
            <div class="main-slider">
                <div class="slider-owl">
                    @foreach($recentPost as $currentPost)
                        <div class="item-photo">
                            <a href="{{ URL('/show'.$currentPost->type.'/'.$currentPost->id )}}"><img  class="img img-responsive" src="{{ asset('ghunghur/public/images/'.$currentPost->type.'/'.$currentPost->image) }}" alt="" /></a>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
        <!-- END .widget -->
    </div>

    <!-- BEGIN .widget -->
    <div class="widget">
        <h3>জনপ্রিয়  লেখা  </h3>
        <div class="widget-content ot-w-article-list">
            @foreach($favouriteArticle as $favouriteArticles)
                <div class="item">
                    <div class="item-header">

                        <a href="{{ URL('/show'.$favouriteArticles->type.'/'.$favouriteArticles->id )}}"><img class="shortStoryImage" src="{{ asset('ghunghur/public/images/'.$favouriteArticles->type.'/'.$favouriteArticles->image) }}" alt="" /></a>
                    </div>
                    <div class="item-content">
                        <h4><a href="{{ URL('/show'.$favouriteArticles->type.'/'.$favouriteArticles->id )}}"> {{ $favouriteArticles->title }} </a></h4>
                        <span class="item-meta"><span class="item-meta-item"><i class="material-icons">access_time</i> {{ $favouriteArticles->published_date }} </span></span>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- END .widget -->
    </div>
    @php
        $new = new \Illuminate\Support\Facades\DB();
        $video = \Illuminate\Support\Facades\DB::table('advertising')->where('type','=', 4)->orderBy('id','desc')->limit('1')->first();
     @endphp 
  @if (\Illuminate\Support\Facades\Route::currentRouteName() == "home")
     <div class="widget">
        <h3> ভিডিও </h3>

        <div class="widget-content">

            <div class="">
                @if(isset($video->url) == true)
                 <iframe width="310" height="210" src="{{ secure_asset($video->url) }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                 @else
                <p class="text-right"> </p>

            @endif
             </div>
        </div>
        <!-- END .widget -->
    </div>
   @endif


@php
    $new = new \Illuminate\Support\Facades\DB();
    $add = \Illuminate\Support\Facades\DB::table('advertising')->where('type','=', 2)->orderBy('id','desc')->limit('2')->get();

@endphp
    <!-- BEGIN .widget -->
   {{-- {{ dd($add) }}--}}
    @foreach ($add as $adv)
    <div class="widget">
        <div class="widget-content">

            {{--{{ dd($adv) }}--}}
            @if(isset($adv->filename)== true)
            <a href="javascript:void(0);" target="_blank"><img src="{{ secure_asset('ghunghur/public/images/Bigapon/'.$adv->filename) }}" alt="" /></a>
                @else
            <p></p>
                @endif
        </div>


        <!-- END .widget -->
    </div>

    @endforeach


    <!-- END #sidebar -->
</aside>

</div>


<!-- END .wrapper -->
</div>


<!-- BEGIN .content -->
</div>

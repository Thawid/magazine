<!-- BEGIN #sidebar -->
<aside id="sidebar">

    <!-- BEGIN .widget -->
    <div class="widget">
        <h3> সাম্প্রতিক লেখা</h3>
        <div class="widget-content ot-w-gallery-list">

                <div class="item">
                    <div class="item-header slider-owl">
                        @foreach($recentPost as $currentPost)
                            <div class="item-photo">
                                <a href="{{ URL('/show'.$currentPost->type.'/'.$currentPost->id )}}"><img  class="img img-responsive" src="{{ asset('ghunghur/public/images/'.$currentPost->type.'/'.$currentPost->image) }}" alt="" /></a>
                            </div>
                        @endforeach
                    </div>

                </div>

        </div>

        <!-- END .widget -->
    </div>

    <!-- BEGIN .widget -->
    <div class="widget">
        <h3>জনপ্রিয়  লেখা </h3>
        <div class="widget-content ot-w-article-list">
            @foreach($favouriteArticle as $favouriteArticles)
                <div class="item">
                    <div class="item-header">

                        <a href="{{ URL('/show'.$favouriteArticles->type.'/'.$favouriteArticles->id )}}"><img class="shortStoryImage" src="{{ asset('ghunghur/public/images/'.$favouriteArticles->type.'/'.$favouriteArticles->image) }}" alt="" /></a>
                    </div>
                    <div class="item-content">
                        <h4><a href="{{ URL('/show'.$favouriteArticles->type.'/'.$favouriteArticles->id )}}"> {{ $favouriteArticles->title }} </a></h4>
                        <span class="item-meta"><span class="item-meta-item"><i class="material-icons">access_time</i> {{ $favouriteArticles->published_date }} </span></span>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- END .widget -->
    </div>
    <div class="widget">
        <h3> ভিডিও </h3>

        <div class="widget-content widget-content-video">

            <div class="item">
                <div class="item-header">

                        <div class="item-photo">
                            <iframe width="315" height="215" src="https://www.youtube.com/embed/PkUU8eDCVSc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>

                </div>

            </div>

        </div>
        <!-- END .widget -->
    </div>

    <!-- END #sidebar -->
</aside>

</div>
{{--<div class="row">
    <div class="col-md-8">
        <div class="disqus" id="disqus_thread"></div>
    </div>
</div>--}}

<!-- END .wrapper -->
</div>


<!-- BEGIN .content -->
</div>
<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/17/2019
 * Time: 4:00 PM
 */
?>


<div class="form-group">
    {{ Form::hidden('type', 'Type') }}
    {{ Form::hidden('type', 'SpecialVolume', ['class' => 'form-control', 'id' => 'type', 'required' => 'required','readonly'=>'on']) }}
</div>

<div class="form-group">
    {{ Form::label('author_name', 'Author Name') }} <em>*</em>
    {{ Form::text('author_name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) }}
</div>

<div class="form-group">
    {{ Form::label('title', 'Title') }} <em>*</em>
    {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
</div>

<div class="form-group">
    {{ Form::label('published_date', 'Published Date') }} <em>*</em>
    {{ Form::text('published_date', null, ['class' => 'form-control', 'id' => 'published_date']) }}
</div>


<div class="form-group">
    {{ Form::label('image', 'Image') }} <em>*</em>
    {{ Form::file('image'),null,['class'=>'form-control', 'id'=>'image'] }}
</div>

<div class="form-group">

    {{Form::textarea('post_body', null, ['class'=>'', 'id'=>'editor1', 'rows'=>'10','cols'=>'80'])}}

</div>

<div class="input-group control-group increment form-group">
    <input type="file" name="filename[]" class="form-control">
    <div class="input-group-btn">
        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
    </div>
</div>
<div class="clone hide">
    <div class="control-group input-group form-group" style="margin-top:10px">
        <input type="file" name="filename[]" class="form-control">
        <div class="input-group-btn">
            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
        </div>
    </div>
</div>

<div class="form-group">
   {{-- {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}--}}
    <button type="submit" class="btn btn-primary" style="">Submit</button>
</div>


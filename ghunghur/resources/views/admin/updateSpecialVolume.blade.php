<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/27/2019
 * Time: 1:13 PM
 */
?>

@extends('admin.layout.master')

@section('title',"GhunGhur || Update")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection




@section('content')
    <div class="container">
        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Update Special Volume
                </div>
                <div class="panel-body">

                    {{ Form::model($updateSpecialVolume, ['url' => route('admin.updateSpecialVolume',$updateSpecialVolume->id), 'method' => 'PUT','files'=> true ]) }}
                         @include('admin._formSpecialVolume')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection




@section('script')
    <script> console.log('Hi!'); </script>
@endsection


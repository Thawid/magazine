@extends('admin.layout.master')

@section('title',"CloudCoder || Firewall")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection



@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add Version Name</h3>
        </div>
        <div class="box-header with-border">


            @if(session()->has('status'))
                <p class="alert alert-info">
                    {{  session()->get('status') }}
                </p>
            @endif
            <div class="col-sm-6">
                <div class="panel panel-default">

                    <div class="panel-body">
                        {{ Form::open(['url' => route('admin.storeVersion'), 'method' => 'POST' ]) }}

                        <div class="form-group">
                            {{ Form::label('name', 'Version Name') }} <em>*</em>
                            {{ Form::text('version_name', null, ['class' => 'form-control', 'id' => 'version_name', 'required' => 'required', 'placeholder' => 'Ex : 2 or 3']) }}

                        </div>


                        <div class="form-group">
                            {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
     </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            Version List

        </div>
        <div class="panel-body">
            @if (count($getVersion))
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Version Name</th>
                            <th>Add Date</th>
                            <th>Status</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php ($count = 1)
                        @foreach($getVersion as $allVersion)

                            <tr>
                                <td>{{ $count++ }}</td>
                                <td>{{ $allVersion->version_name }}</td>
                                <td>{{ $allVersion->date }}</td>
                                <td>{{ $allVersion->status }}</td>

                                <td>
                                    <form action="{{ route('admin.deleteVersion', $allVersion->id) }}" method="POST" style="display:inline-block">
                                        {{ csrf_field() }}
                                        <button onclick="return confirm('Are you want to delete this IP?');" class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash "></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                   {{-- {{ $allVersion->links() }}--}}
                </div>
            @else
                <p class="alert alert-info">
                    No Listing Found
                </p>
            @endif
        </div>
    </div>
@endsection

@section('script')
    <script> console.log('Hi!'); </script>
@endsection
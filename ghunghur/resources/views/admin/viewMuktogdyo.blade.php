<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/16/2019
 * Time: 4:50 PM
 */
?>

@extends('admin.layout.master')

@section('title',"GhunGhur || View MuktoGdyo")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')

    @if(session()->has('status'))
        <p class="alert alert-info">
            {{  session()->get('status') }}
        </p>
    @endif
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{--<a href="{{ route('users.index') }}" class="btn btn-success btn-xs">Back</a> --}}
                 Details
                <a style="margin-left: 776px;" href="{{ route('AllMuktogdyo') }}" class="btn-primary btn-sm">সব পোস্ট</a>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Author Name</label>
                        <div class="col-sm-9">
                            <p class="form-control">{{ $viewMuktogdyo->author_name }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Title</label>
                        <div class="col-sm-9">
                            <p class="form-control">{{ $viewMuktogdyo->title }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Published</label>
                        <div class="col-sm-9">
                            <p class="form-control">{{ $viewMuktogdyo->published_date }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Painting Content</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="10" cols="20">{!! strip_tags($viewMuktogdyo->post_body)  !!}</textarea>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection



<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/16/2019
 * Time: 3:10 PM
 */
?>


@extends('admin.layout.master')

@section('title',"GhunGhur || All MuktoGdyo")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection



@section('content')

    <div class="col-sm-12">

        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                All MuktoGdyo
                <a style="margin-left: 776px;" href="{{ route('admin.AddMuktogdyo') }}" class="btn btn-primary btn-xs">  নতুন মুক্তগদ্য  </a>
            </div>
            <div class="panel-body">

                <table class="table table-hover table-bordered" id="table">
                    <thead>

                    <th>Author Name</th>
                    <th>Painting Name</th>
                    <th>Published Date</th>
                    <th> Details</th>
                    <th>Image</th>
                    <th>Action</th>

                    </thead>


                </table>
            </div>
        </div>
    </div>


@stop


@section('script')
    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>

    <script>
        $('#table').DataTable({
            "processing": true,
            "ajax": {
                "url": '{{route('getAllMuktogdyo')}}',
                "type": "get"
            }
        });
    </script>


@endsection



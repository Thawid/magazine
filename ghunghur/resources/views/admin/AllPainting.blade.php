
@extends('admin.layout.master')

@section('title',"GhunGhur || All Painting")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection



@section('content')

    <div class="col-sm-12">

        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                All Published Painting
                <a style="margin-left: 776px;" href="{{ route('admin.AddPainting') }}" class="btn btn-primary btn-xs"> New Painting </a>
            </div>
            <div class="panel-body">

                <table class="table table-hover table-bordered" id="table">
                    <thead>

                        <th>Author Name</th>
                        <th>Painting Name</th>
                        <th>Published Date</th>
                        <th>Painting Details</th>
                        <th>Image</th>
                        <th>Action</th>

                    </thead>


                </table>
            </div>
        </div>
    </div>


@stop


@section('script')
    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>

    <script>
        $('#table').DataTable({
            "processing": true,
            "ajax": {
                "url": '{{route('getAllPainting')}}',
                "type": "get"
            }
        });
    </script>


@endsection


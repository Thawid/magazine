<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/30/2019
 * Time: 3:16 PM
 */
?>


<div class="form-group">
    {{ Form::label('type', 'Advertising Place ') }} <em>*</em>

    {{ Form::select('type',  array('' => 'Select One','1' => 'Header', '2' => 'Side-Bar', '3' => 'Order Now','4' => 'Youtube'),null,['class' => 'form-control', 'id'=>'type']) }}
</div>
<div class="input-group control-group increment form-group">
    <input type="file" name="filename[]" class="form-control">
    <div class="input-group-btn">
        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
    </div>
</div>
<div class="clone hide">
    <div class="control-group input-group form-group" style="margin-top:10px">
        <input type="file" name="filename[]" class="form-control">
        <div class="input-group-btn">
            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
        </div>
    </div>
</div>

    <div class="form-group">
            {{ Form::label('url', 'URL') }} <em>*</em>
            {{ Form::text('url', null, ['class' => 'form-control', 'id' => 'url']) }}
    </div>

<div class="form-group">
    {{-- {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}--}}
    <button type="submit" class="btn btn-primary" style="">Submit</button>
</div>

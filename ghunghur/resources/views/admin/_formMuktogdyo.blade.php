<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/16/2019
 * Time: 1:15 PM
 */
?>





<div class="form-group">
    {{ Form::hidden('type', 'Type') }}
    {{ Form::hidden('type', 'Muktogdyo', ['class' => 'form-control', 'id' => 'type', 'required' => 'required','readonly'=>'on']) }}
</div>

<div class="form-group">
    {{ Form::label('author_name', 'Author Name') }} <em>*</em>
    {{ Form::text('author_name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) }}
</div>

<div class="form-group">
    {{ Form::label('title', 'Title') }} <em>*</em>
    {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
</div>

<div class="form-group">
    {{ Form::label('published_date', 'Published Date') }} <em>*</em>
    {{ Form::text('published_date', null, ['class' => 'form-control', 'id' => 'published_date']) }}
</div>


<div class="form-group">
    {{ Form::label('image', 'Image') }} <em>*</em>
    {{ Form::file('image'),null,['class'=>'form-control', 'id'=>'image'] }}
</div>

<div class="form-group">

    {{Form::textarea('post_body', null, ['class'=>'', 'id'=>'editor1', 'rows'=>'10','cols'=>'80'])}}

</div>



<div class="form-group">
    {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
</div>



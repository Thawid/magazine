<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/16/2019
 * Time: 5:45 PM
 */
?>

@extends('admin.layout.master')

@section('title',"GhunGhur || Shaity-Songbad")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>  সাহিত্য সংবাদ </small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home </a></li>
    <li class="active"> সাহিত্য সংবাদ  </li>
@endsection


@section('content')
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h1 class="box-title"> সাহিত্য সংবাদ  </h1>
            </div>
            <div class="box-header with-border">


                @if(session()->has('status'))
                    <p class="alert alert-info">
                        {{  session()->get('status') }}
                    </p>
                @endif
                <div class="col-sm-10">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            {{ Form::open(['url' => route('admin.StoreSahitySongbad'), 'method' => 'POST', 'files'=>true ]) }}
                                 @include('admin._formSahitySongbad')
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
@stop




@section('script')
    <script> console.log('Hi!'); </script>
@endsection


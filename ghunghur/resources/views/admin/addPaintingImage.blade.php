<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/30/2019
 * Time: 2:39 PM
 */
?>



@extends('admin.layout.master')

@section('title',"GhunGhur || Create Poeam")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home </a></li>
    <li class="active"> Create New poeam </li>
@endsection


@section('content')
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h1 class="box-title"> চিত্রকলা </h1>
            </div>
            <div class="box-header with-border">


                @if(session()->has('status'))
                    <p class="alert alert-info">
                        {{  session()->get('status') }}
                    </p>
                @endif
                <div class="col-sm-10">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <h3 class="jumbotron">Drag and Drop Image here to upload</h3>
                            <form method="post" action="{{url('admin/storePainting')}}" enctype="multipart/form-data"
                                  class="dropzone" id="dropzone">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
@stop


<script type="text/javascript">
    Dropzone.options.dropzone =
        {
            maxFiles: 10,
            maxFilesize: 12,
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
                return time+file.name;
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            timeout: 50000,
            removedfile: function(file)
            {
                var name = file.upload.filename;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    type: 'POST',
                    url: '{{ url("image/delete") }}',
                    data: {filename: name},
                    success: function (data){
                        console.log("File has been successfully removed!!");
                    },
                    error: function(e) {
                        console.log(e);
                    }});
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },

            success: function(file, response)
            {
                console.log(response);
            },
            error: function(file, response)
            {
                return false;
            }
        };
</script>

@section('script')
    <script> console.log('Hi!'); </script>
@endsection

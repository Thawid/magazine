@extends('admin.layout.master')

@section('title',"GhunGhur || Admin")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')
    Admin Dashboard


@endsection


@section('script')
    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
    <script> console.log('Hi!'); </script>

@endsection
<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/17/2019
 * Time: 3:59 PM
 */
?>


@extends('admin.layout.master')

@section('title',"GhunGhur || Special Volume")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>  বিশেষ সংখ্যা </small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home </a></li>
    <li class="active">  বিশেষ সংখ্যা  </li>
@endsection


@section('content')
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h1 class="box-title">  বিশেষ সংখ্যা  </h1>
            </div>
            <div class="box-header with-border">


                @if(session()->has('status'))
                    <p class="alert alert-info">
                        {{  session()->get('status') }}
                    </p>
                @endif
                <div class="col-sm-10">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            {{ Form::open(['url' => route('admin.StoreSpecialVolume'), 'method' => 'POST', 'files'=>true ]) }}
                                @include('admin._formSpecialVolume')
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
@stop




@section('script')
    <script>
        $(document).ready(function() {

            $(".btn-success").click(function(){
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click",".btn-danger",function(){
                $(this).parents(".control-group").remove();
            });

        });
    </script>
@endsection

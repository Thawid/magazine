<?php
/**
 * Created by PhpStorm.
 * User: tanvi
 * Date: 11/18/2018
 * Time: 3:49 PM
 */
?>

@extends('admin.layout.master')

@section('title',"GhunGhur || View Drama Details")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')

    @if(session()->has('status'))
        <p class="alert alert-info">
            {{  session()->get('status') }}
        </p>
    @endif
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{--<a href="{{ route('users.index') }}" class="btn btn-success btn-xs">Back</a> --}}
                Drama Details
                <a style="margin-left: 776px;" href="{{ route('AllDrama') }}" class="btn-primary btn-sm">All Drama</a>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Author Name</label>
                        <div class="col-sm-9">
                            <p class="form-control">{{ $viewDrama->author_name }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Drama Name</label>
                        <div class="col-sm-9">
                            <p class="form-control">{{ $viewDrama->title }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Published</label>
                        <div class="col-sm-9">
                            <p class="form-control">{{ $viewDrama->published_date }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Drama Content</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="10" cols="20">{!! strip_tags($viewDrama->post_body)  !!}</textarea>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection


@extends('admin.layout.master')

@section('title',"CloudCoder || Firewall")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small> Admin Dashboard </small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')


    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Login Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover table-bordered" id="table">
                <thead>
                    <th> Client Name </th>
                    <th> Login IP </th>
                    <th> Login Date </th>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
    <script> console.log('Hi!'); </script>
    <script>
       /* $(document).ready(function () {
            $('#table').DataTable();
        });*/
    </script>
    <script>
        $('#table').DataTable({
            "processing": true,
            "ajax": {
                "url": '{{route('getAllUserLoginHistory')}}',
                "type": "get"
            }
        });
    </script>

@endsection


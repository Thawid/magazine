<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/27/2019
 * Time: 6:44 PM
 */
?>


@extends('admin.layout.master')

@section('title',"GhunGhur ||All Rehearse")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection



@section('content')

    <div class="col-sm-12">

        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                User Profile

            </div>
            <div class="panel-body">
                @if (count($profile))
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($profile as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>

                                    <td>
                                        
                                        <a href="{{route('admin.editProfile', $user->id) }}" class="btn btn-success btn-xs">Edit</a>


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                @else
                    <p class="alert alert-info">
                        No Listing Found
                    </p>
                @endif
            </div>
        </div>
    </div>


@stop


@section('script')
    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
    
@endsection

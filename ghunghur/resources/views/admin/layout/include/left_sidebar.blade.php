<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--<li class="header">HEADER</li>-->
            <!-- Optionally, you can add icons to the links -->
            <li  class="active">
                <a href="{{url('admin/home')}}">
                    <i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span>
                </a>
            </li>
            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> প্রবন্ধ </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllArtical')) class="active" @endif><a
                                href="{{url('admin/AllArtical')}}"><i class="fa fa-list"></i> All Artical </a>
                    </li>
                    <li @if(URL::current()==url('admin.Artical')) class="active" @endif><a
                                href="{{url('admin/Artical')}}"><i class="fa fa-plus-square-o"></i> New Artical </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> কবিতা </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllPoeams')) class="active" @endif><a
                                href="{{url('admin/AllPoeams')}}"><i class="fa fa-list"></i> All Poeams </a>
                    </li>
                    <li @if(URL::current()==url('admin.AddPoeams')) class="active" @endif><a
                                href="{{url('admin/AddPoeams')}}"><i class="fa fa-plus-square-o"></i> New Poeams </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> ছোট গল্প </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllStory')) class="active" @endif><a
                                href="{{url('admin/AllStory')}}"><i class="fa fa-list"></i> All Short Story </a>
                    </li>
                    <li @if(URL::current()==url('admin.AddStory')) class="active" @endif><a
                                href="{{url('admin/AddStory')}}"><i class="fa fa-plus-square-o"></i> New Short Story </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> ধারাবাহিক </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllSeries')) class="active" @endif><a
                                href="{{url('admin/AllSeries')}}"><i class="fa fa-list"></i> All Series </a>
                    </li>
                    <li @if(URL::current()==url('admin.AddSeries')) class="active" @endif><a
                                href="{{url('admin/AddSeries')}}"><i class="fa fa-plus-square-o"></i> New Series </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> সাক্ষাৎকার </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllInterview')) class="active" @endif><a
                                href="{{url('admin/AllInterview')}}"><i class="fa fa-list"></i> All Interviews </a>
                    </li>
                    <li @if(URL::current()==url('admin.AddInterview')) class="active" @endif><a
                                href="{{url('admin/AddInterview')}}"><i class="fa fa-plus-square-o"></i> New Interview </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> অনুবাদ </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllTranslation')) class="active" @endif><a
                                href="{{url('admin/AllTranslation')}}"><i class="fa fa-list"></i> All Translation </a>
                    </li>
                    <li @if(URL::current()==url('admin.AddTranslation')) class="active" @endif><a
                                href="{{url('admin/AddTranslation')}}"><i class="fa fa-plus-square-o"></i> New Translation </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> সাহিত্য সংবাদ  </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllSahitySongbad')) class="active" @endif><a
                                href="{{url('admin/AllSahitySongbad')}}"><i class="fa fa-list"></i> All Post </a>
                    </li>
                    <li @if(URL::current()==url('admin.AddSahitySongbad')) class="active" @endif><a
                                href="{{url('admin/AddSahitySongbad')}}"><i class="fa fa-plus-square-o"></i> New Post </a>
                    </li>

                </ul>
            </li>





            <li class="treeview">
                <a href="#">
                    <i class="fa fa-share"></i> <span>Multilevel</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">

                    <li class="treeview">
                        <a href="javascript:void(0);">
                            <i class="fa fa-book"></i>
                            <span> মুক্তগদ্য  </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/AddMuktogdyo') }}"><i class="fa fa-circle-o"></i> New Post </a></li>
                            <li><a href="{{ url('admin/AllMuktogdyo') }}"><i class="fa fa-circle-o"></i> All Post </a></li>

                        </ul>

                    </li>

                    <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
                    class="treeview" @endif>
                        <a href="javascript:void(0);">
                            <i class="fa fa-book"></i>
                            <span> চলচ্চিত্র </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if(URL::current()==url('admin/AllMovies')) class="active" @endif><a
                                        href="{{url('admin/AllMovies')}}"><i class="fa fa-list"></i> All Movies </a>
                            </li>
                            <li @if(URL::current()==url('admin.AddMovie')) class="active" @endif><a
                                        href="{{url('admin/AddMovie')}}"><i class="fa fa-plus-square-o"></i> New Movies </a>
                            </li>

                        </ul>
                    </li>

                    <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
                    class="treeview" @endif>
                        <a href="javascript:void(0);">
                            <i class="fa fa-book"></i>
                            <span> নাটক </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if(URL::current()==url('admin/AllDrama')) class="active" @endif><a
                                        href="{{url('admin/AllDrama')}}"><i class="fa fa-list"></i> All Drama </a>
                            </li>
                            <li @if(URL::current()==url('admin.AddDrama')) class="active" @endif><a
                                        href="{{url('admin/AddDrama')}}"><i class="fa fa-plus-square-o"></i> New Drama </a>
                            </li>

                        </ul>
                    </li>

                    <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
                    class="treeview" @endif>
                        <a href="javascript:void(0);">
                            <i class="fa fa-book"></i>
                            <span> চিত্রকলা </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if(URL::current()==url('admin/AllPainting')) class="active" @endif><a
                                        href="{{url('admin/AllPainting')}}"><i class="fa fa-list"></i> All Painting </a>
                            </li>
                            <li @if(URL::current()==url('admin.AddPainting')) class="active" @endif><a
                                        href="{{url('admin/AddPainting')}}"><i class="fa fa-plus-square-o"></i> New Painting </a>
                            </li>

                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> <span> পুনর্পাঠ </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/AddRehearse') }}"><i class="fa fa-circle-o"></i> New Post</a></li>
                            <li><a href="{{ url('admin/AllRehearse') }}"><i class="fa fa-circle-o"></i> All Post </a></li>

                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> <span> বইপত্র </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/AddBooks') }}"><i class="fa fa-circle-o"></i> New Post </a></li>
                            <li><a href="{{ url('admin/AllBooks') }}"><i class="fa fa-circle-o"></i> All Post</a></li>

                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> <span> ছড়া </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/AddRhyme') }}"><i class="fa fa-circle-o"></i> New Post </a></li>
                            <li><a href="{{ url('admin/AllRhyme') }}"><i class="fa fa-circle-o"></i> All Post </a></li>

                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> <span> অন্যান </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/AddOthers') }}"><i class="fa fa-circle-o"></i> New Post </a></li>
                            <li><a href="{{ url('admin/AllOthers') }}"><i class="fa fa-circle-o"></i> All Post </a></li>

                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> <span> বই ঘর </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/AddBookHouse') }}"><i class="fa fa-circle-o"></i> New Post </a></li>
                            <li><a href="{{ url('admin/AllBookHouse') }}"><i class="fa fa-circle-o"></i> All Post </a></li>

                        </ul>
                    </li>



                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> <span> ই-ঘুংঘুর </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/AddArchive') }}"><i class="fa fa-circle-o"></i> New Post </a></li>
                            <li><a href="{{ url('admin/AllArchive') }}"><i class="fa fa-circle-o"></i> All Archive </a></li>

                        </ul>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-share"></i> <span>Multilevel</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">



                    <li class="treeview">
                        <a href="javascript:void(0);">
                            <i class="fa fa-book"></i>
                            <span> আলোকচিত্র </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/fileCreate') }}"><i class="fa fa-circle-o"></i> Upload Image </a></li>
                            <li><a href="{{ url('admin/viewHomePhotography') }}"><i class="fa fa-circle-o"></i> All Image </a></li>

                        </ul>

                    </li>

                    <li class="treeview">
                        <a href="javascript:void(0);">
                            <i class="fa fa-book"></i>
                            <span> চিত্রকলা </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/addPaintingImage') }}"><i class="fa fa-circle-o"></i> Upload Image </a></li>
                            <li><a href="{{ url('admin/viewHomePainting') }}"><i class="fa fa-circle-o"></i> All Image </a></li>

                        </ul>

                    </li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i><span> বিজ্ঞাপন </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/addAdvertising') }}"><i class="fa fa-circle-o"></i>  Upload Image </a></li>
                            <li><a href="{{ url('admin/viewAdvertising') }}"><i class="fa fa-circle-o"></i> All Image </a></li>

                        </ul>

                    </li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i><span> ছবির লিংক  </span>
                            <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/addImage') }}"><i class="fa fa-circle-o"></i>  Upload Image </a></li>
                            <li><a href="{{ url('admin/viewImageLink') }}"><i class="fa fa-circle-o"></i> All Image </a></li>

                        </ul>

                    </li>
                </ul>
            </li>


        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

<!-- Main Header -->
<header class="main-header">
    <!-- Logo -->
    <div class="no-padding hidden-xs">

        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">
            <b class="">GG</b>

        </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg text-left"><b> GhunGhur</b>

        </span>

        </a>
    </div>


    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        মুক্তগদ্য
                    </a>
                    <ul class="dropdown-menu">

                        <li class="footer" @if(URL::current()==url('admin/AddMuktogdyo')) class="active" @endif><a href="{{url('admin/AddMuktogdyo')}}"> নতুন মুক্তগদ্য </a>  </li>

                        <li class="footer" @if(URL::current()==url('admin/AllMuktogdyo')) class="active" @endif><a
                                    href="{{url('admin/AllMuktogdyo')}}"> সব পোস্ট  </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        সাহিত্য সংবাদ
                    </a>
                    <ul class="dropdown-menu">
                        <li class="footer" @if(URL::current()==url('admin/AddSahitySongbad')) class="active" @endif><a href="{{url('admin/AddSahitySongbad')}}"> নতুন সাহিত্য সংবাদ  </a>  </li>

                        <li class="footer" @if(URL::current()==url('admin/AllSahitySongbad')) class="active" @endif><a
                                    href="{{url('admin/AllSahitySongbad')}}"> সব পোস্ট  </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        বিশেষ সংখ্যা
                    </a>
                    <ul class="dropdown-menu">
                        <li class="footer" @if(URL::current()==url('admin/AddSpecialVolume')) class="active" @endif><a href="{{url('admin/AddSpecialVolume')}}"> নতুন বিশেষ সংখ্যা </a>  </li>

                        <li class="footer" @if(URL::current()==url('admin/AllSpecialVolume')) class="active" @endif><a
                                    href="{{url('admin/AllSpecialVolume')}}"> সব পোস্ট  </a>
                        </li>
                        <li class="footer" @if(URL::current()==url('admin/SpecialVolumeList')) class="active" @endif><a
                                    href="{{url('admin/SpecialVolumeList')}}"> সব গুলো সংখ্যা   </a>
                        </li>
                    </ul>
                </li>


                <li class="dropdown user user-menu">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        <!-- The user image in the navbar-->
                        <i class="fa fa-user"></i>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{auth()->user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">

                            <img src="{{asset('assets/img/user2-160x160.jpg')}}" class="img-circle"
                                 alt="User Image')}}">
                            <p>
                                {{auth()->user()->email}}

                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{url('admin/shoProfile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                   class="btn btn-default btn-flat" href="{{ url('admin/logout') }}">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ url('admin/logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/28/2019
 * Time: 3:52 PM
 */
?>


<div class="form-group">
    {{ Form::hidden('type', 'Type') }}
    {{ Form::hidden('type', 'Archive', ['class' => 'form-control', 'id' => 'type', 'required' => 'required','readonly'=>'on']) }}
</div>

<div class="form-group">
    {{ Form::label('number', 'Number') }} <em>*</em>
    {{ Form::text('number', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) }}
</div>

<div class="form-group">
    {{ Form::label('title', 'Title') }} <em>*</em>
    {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
</div>

<div class="form-group">
    {{ Form::label('published_date', 'Published Date') }} <em>*</em>
    {{ Form::text('published_date', null, ['class' => 'form-control', 'id' => 'published_date']) }}
</div>


<div class="form-group">
    {{ Form::label('image', 'Image') }} <em>*</em>
    {{ Form::file('image'),null,['class'=>'form-control', 'id'=>'image'] }}
</div>


<div class="input-group control-group increment form-group">
    <input type="file" name="filename[]" class="form-control">
    <div class="input-group-btn">
        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
    </div>
</div>


<div class="form-group">
    {{-- {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}--}}
    <button type="submit" class="btn btn-primary" style="">Submit</button>
</div>

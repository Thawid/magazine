<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/30/2019
 * Time: 3:14 PM
 */
?>

@extends('admin.layout.master')

@section('title',"GhunGhur || Special Volume")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small> বিজ্ঞাপন </small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home </a></li>
    <li class="active">   বিজ্ঞাপন </li>
@endsection


@section('content')
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h1 class="box-title">  বিজ্ঞাপন  </h1>
            </div>
            <div class="box-header with-border">


                @if(session()->has('status'))
                    <p class="alert alert-info">
                        {{  session()->get('status') }}
                    </p>
                @endif
                <div class="col-sm-10">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            {{ Form::open(['url' => route('admin.storeAdvertising'), 'method' => 'POST', 'files'=>true ]) }}
                                @include('admin._formAdvertising')
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
@stop




@section('script')
    <script>
        $(document).ready(function() {

            $(".btn-success").click(function(){
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click",".btn-danger",function(){
                $(this).parents(".control-group").remove();
            });

        });
    </script>
@endsection


@extends('admin.layout.master')

@section('title',"GhunGhur || Article Update")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection




@section('content')
    <div class="container">
        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Update Painting
                </div>
                <div class="panel-body">

                    {{ Form::model($updatePainting, ['url' => route('admin.updatePainting',$updatePainting->id), 'method' => 'PUT','files'=> true ]) }}
                         @include('admin._formPainting')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection




@section('script')
    <script> console.log('Hi!'); </script>
@endsection

<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/27/2019
 * Time: 4:36 PM
 */
?>

@extends('admin.layout.master')

@section('title',"GhunGhur||Update Rehearse")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection




@section('content')
    <div class="container">
        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Update Rehearse
                </div>
                <div class="panel-body">

                    {{ Form::model($updateRehearse, ['url' => route('admin.updateRehearse',$updateRehearse->id), 'method' => 'PUT','files'=> true ]) }}
                            @include('admin._formRehearse')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection




@section('script')
    <script> console.log('Hi!'); </script>
@endsection

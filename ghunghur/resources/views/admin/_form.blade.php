



                <div class="form-group">
                    {{ Form::label('type', 'Type') }} <em>*</em>
                    {{ Form::text('type', 'Article', ['class' => 'form-control', 'id' => 'type', 'required' => 'required','readonly'=>'on']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('author_name', 'Author Name') }} <em>*</em>
                    {{ Form::text('author_name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('title', 'Title') }} <em>*</em>
                    {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('published_date', 'Published Date') }} <em>*</em>
                    {{ Form::text('published_date', null, ['class' => 'form-control', 'id' => 'published_date']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('favourite', 'Favourite Article') }} <em>*</em>
                    {{ Form::select('favourite',  array('0' => 'Regular Article', '1' => 'Favourite Article'),null,['class' => 'form-control', 'id'=>'favourite']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('image', 'Article Image') }} <em>*</em>
                    {{ Form::file('image'),null,['class'=>'form-control', 'id'=>'image'] }}
                </div>
                <div class="form-group">
                    {{Form::textarea('post_body', null, ['class'=>'', 'id'=>'editor1', 'rows'=>'10','cols'=>'80'])}}

                </div>



                <div class="form-group">
                    {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
                </div>


<?php
/**
 * Created by Tanvir.
 * User: Tanvir
 * Date: 3/28/2019
 * Time: 2:20 PM
 */
?>

<div class="form-group">
    {{ Form::hidden('type', 'Type') }}
    {{ Form::hidden('type', 'BookHouse', ['class' => 'form-control', 'id' => 'type', 'required' => 'required','readonly'=>'on']) }}
</div>

<div class="form-group">
    {{ Form::label('author_name', 'Author Name') }} <em>*</em>
    {{ Form::text('author_name', null, ['class' => 'form-control', 'id' => 'name', 'required' => 'required']) }}
</div>

<div class="form-group">
    {{ Form::label('title', 'Book Name') }} <em>*</em>
    {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
</div>




<div class="form-group">
    {{ Form::label('image', 'Image') }} <em>*</em>
    {{ Form::file('image'),null,['class'=>'form-control', 'id'=>'image'] }}
</div>


<div class="form-group">
    {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
</div>
